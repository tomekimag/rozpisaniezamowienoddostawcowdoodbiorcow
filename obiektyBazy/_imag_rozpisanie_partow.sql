--drop table _imag_rozpisanie_partow
create table _imag_rozpisanie_partow
(
	id bigint not null identity(1, 1) primary key,
	id_poz_zam_przychod numeric not null,
	idLokalizacji numeric not null,
	Part varchar(100) not null,
	Rozchod decimal(16, 6) not null,
	id_poz_zam_rozchod numeric null,
	id_uzytkownika numeric not null
)
go

alter table _imag_rozpisanie_partow add historyczny tinyint not null default(0)
go

alter table _imag_rozpisanie_partow add PartPierwotny varchar(100) not null default('')
go
