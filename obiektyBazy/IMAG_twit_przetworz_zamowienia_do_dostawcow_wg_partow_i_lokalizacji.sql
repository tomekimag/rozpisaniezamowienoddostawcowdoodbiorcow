ALTER procedure [dbo].[IMAG_twit_przetworz_zamowienia_do_dostawcow_wg_partow_i_lokalizacji]
@IdFirmy numeric, @IdMagazynu numeric, @IdUzytkownika numeric, @idZamowienia numeric
as
begin

	set xact_abort on
	set transaction isolation level REPEATABLE READ
	begin transaction

	declare @NUMER varchar(100)

	declare @id_pracownika numeric

	declare @Data Int

	declare @ID_ARTYKULU numeric
	declare @KOD_VAT varchar(3)
	declare @ZAMOWIONO decimal(16, 6)
	declare @ZREALIZOWANO decimal(16, 6)
	declare @ZAREZERWOWANO decimal(16, 6)
	declare @DO_REZERWACJI decimal(16, 6)
	declare @CENA_NETTO decimal(14, 4)
	declare @CENA_BRUTTO decimal(14, 4)
	declare @CENA_NETTO_WAL decimal(14, 4)
	declare @CENA_BRUTTO_WAL decimal(14, 4)
	declare @PRZELICZNIK decimal(16, 6)
	declare @NARZUT decimal(8, 4)
	declare @OPIS varchar(500)
	declare @znak_narzutu tinyint
	declare @TRYB_REJESTRACJI tinyint
	declare @ID_DOSTAWY_REZ numeric
	declare @ID_WARIANTU_PRODUKTU numeric
	declare @ZNACZNIK_CENY char(1)

	declare @ID_JEDNOSTKI numeric, @IdKontrahenta numeric

	declare @RetStat int

	declare @POLE1 varchar(100)
	declare @POLE1_pozycja varchar(100), @POLE8_pozycja varchar(100)

	declare @id_pozycji_zamowienia numeric

	declare @kod_katalogowy varchar(20), @kod_handlowy varchar(20), @nazwa varchar(40), @jednostka varchar(10), @PRODUCENT varchar(50), @KRAJ_POCHODZENIA varchar(3),
		@KATEGORIA varchar(50), @KATEGORIA_WIELOPOZIOMOWA varchar(50), @JEDNOSTKA_SKROT varchar(10), @VAT_ZAKUPU char(3), @VAT_SPRZEDAZY char(3), @STAN_MINIMALNY decimal(16, 6),
		@STAN_MAKSYMALNY decimal(16, 6), @JED_WAGI varchar(10), @WAGA decimal(20, 3), @JED_WYMIARU varchar(10), @WYMIAR_W decimal(20, 3), @WYMIAR_S decimal(20, 3), @WYMIAR_G decimal(20, 3),
		@ilosc_sztuk decimal(16, 6), @pole1_karton varchar(100), @ID_POZYCJI_ZAMOWIENIA_stara numeric

	declare @id bigint, @id_poz_zam_przychod numeric, @idLokalizacji numeric, @Part varchar(100), @Rozchod decimal(16, 6), @id_poz_zam_rozchod numeric

	declare @autonumer int	
	select top 1 @NUMER = NUMER, @Data = DATA, @IdKontrahenta = ID_KONTRAHENTA, @autonumer = AUTONUMER from ZAMOWIENIE where ID_ZAMOWIENIA = @idZamowienia

	declare @tabelaZPozycjamiZamowienia table
	(
		ID_POZYCJI_ZAMOWIENIA numeric, 
		ID_ARTYKULU numeric, 
		KOD_VAT varchar(3), 
		ZAMOWIONO decimal(16, 6),
		ZREALIZOWANO decimal(16, 6), 
		ZAREZERWOWANO decimal(16, 6), 
		DO_REZERWACJI decimal(16, 6), 
		CENA_NETTO decimal(14, 4), 
		CENA_BRUTTO decimal(14, 4), 
		CENA_NETTO_WAL decimal(14, 4), 
		CENA_BRUTTO_WAL decimal(14, 4), 
		PRZELICZNIK decimal(16, 6), 
		JEDNOSTKA varchar(10), 
		NARZUT decimal(8, 4), 
		OPIS varchar(500)
		)

	insert into @tabelaZPozycjamiZamowienia
	select PZ.ID_POZYCJI_ZAMOWIENIA, PZ.ID_ARTYKULU, PZ.KOD_VAT, PZ.ZAMOWIONO,
		PZ.ZREALIZOWANO, PZ.ZAREZERWOWANO, PZ.DO_REZERWACJI, PZ.CENA_NETTO, PZ.CENA_BRUTTO, PZ.CENA_NETTO_WAL, 
		PZ.CENA_BRUTTO_WAL, PZ.PRZELICZNIK, PZ.JEDNOSTKA, PZ.NARZUT, PZ.OPIS
	from POZYCJA_ZAMOWIENIA PZ
	where PZ.ID_ZAMOWIENIA = @idZamowienia
	and exists (select 1 from _imag_rozpisanie_partow where id_uzytkownika = @IdUzytkownika and id_poz_zam_przychod = PZ.ID_POZYCJI_ZAMOWIENIA)


	declare kursor_po_poz cursor local fast_forward for
	select ID_POZYCJI_ZAMOWIENIA, ID_ARTYKULU, KOD_VAT, ZAMOWIONO,
		ZREALIZOWANO, ZAREZERWOWANO, DO_REZERWACJI, CENA_NETTO, CENA_BRUTTO, CENA_NETTO_WAL, 
		CENA_BRUTTO_WAL, PRZELICZNIK, JEDNOSTKA, NARZUT, OPIS
	from @tabelaZPozycjamiZamowienia
	
	OPEN kursor_po_poz
	
	FETCH NEXT FROM kursor_po_poz into @ID_POZYCJI_ZAMOWIENIA_stara, @ID_ARTYKULU, @KOD_VAT, @ZAMOWIONO,
		@ZREALIZOWANO, @ZAREZERWOWANO, @DO_REZERWACJI, @CENA_NETTO, @CENA_BRUTTO, @CENA_NETTO_WAL, 
		@CENA_BRUTTO_WAL, @PRZELICZNIK, @JEDNOSTKA, @NARZUT, @OPIS
	
	WHILE @@FETCH_STATUS = 0
	BEGIN

		if (select COUNT(1) from _imag_rozpisanie_partow where id_uzytkownika = @IdUzytkownika and id_poz_zam_przychod = @ID_POZYCJI_ZAMOWIENIA_stara) = 1
		begin
			select @Part = Part, @idLokalizacji = idLokalizacji, @id_poz_zam_rozchod = id_poz_zam_rozchod
			from _imag_rozpisanie_partow where id_uzytkownika = @IdUzytkownika and id_poz_zam_przychod = @ID_POZYCJI_ZAMOWIENIA_stara

			update POZYCJA_ZAMOWIENIA set
				POLE5 = @Part,
				POLE6 = cast(@idLokalizacji as varchar(100))
			where ID_POZYCJI_ZAMOWIENIA = @ID_POZYCJI_ZAMOWIENIA_stara

			if isNull(@id_poz_zam_rozchod, 0) != 0
				update POZYCJA_ZAMOWIENIA set
					POLE5 = @Part,
					POLE6 = cast(@idLokalizacji as varchar(100))
				where ID_POZYCJI_ZAMOWIENIA = @id_poz_zam_rozchod
		end
		else
		begin

	
			declare kursor_po_poz_rozpiski cursor local fast_forward for
			select id, id_poz_zam_przychod, idLokalizacji, Part, Rozchod, id_poz_zam_rozchod
			from _imag_rozpisanie_partow where id_uzytkownika = @IdUzytkownika and id_poz_zam_przychod = @ID_POZYCJI_ZAMOWIENIA_stara
			
			OPEN kursor_po_poz_rozpiski
	
			FETCH NEXT FROM kursor_po_poz_rozpiski into @id, @id_poz_zam_przychod, @idLokalizacji, @Part, @Rozchod, @id_poz_zam_rozchod

			WHILE @@FETCH_STATUS = 0
			BEGIN
				
				set @ZAMOWIONO = @Rozchod
				set @ZREALIZOWANO = 0
				set @NARZUT = 0
				set @OPIS = ''
				set @znak_narzutu = 2
				set @TRYB_REJESTRACJI = 0
				set @ID_DOSTAWY_REZ = 0
				set @ID_WARIANTU_PRODUKTU = 0
				set @ZNACZNIK_CENY = 0
				set @DO_REZERWACJI = 0
				set @ZAREZERWOWANO = 0
				set @CENA_NETTO = 0
				set @CENA_BRUTTO = 0
				set @CENA_NETTO_WAL = 0
				set @CENA_BRUTTO_WAL = 0
				set @PRZELICZNIK = 1
				set @ID_WARIANTU_PRODUKTU = 0

				exec @RetStat = RM_DodajPozycjeZamowienia_Server @id_pozycji_zamowienia output, @idZamowienia, @ID_ARTYKULU, @KOD_VAT, @ZAMOWIONO,
					@ZREALIZOWANO, @ZAREZERWOWANO, @DO_REZERWACJI, @CENA_NETTO, @CENA_BRUTTO, @CENA_NETTO_WAL,
					@CENA_BRUTTO_WAL, @PRZELICZNIK, @JEDNOSTKA, @NARZUT, @OPIS, @znak_narzutu, @TRYB_REJESTRACJI, 
					@ID_DOSTAWY_REZ, @ID_WARIANTU_PRODUKTU, @ZNACZNIK_CENY

				if @RetStat = 0 goto Error
				else
				begin
					update _imag_rozpisanie_partow set
						id_poz_zam_przychod = @id_pozycji_zamowienia
					where id_uzytkownika = @IdUzytkownika and id = @id

					update POZYCJA_ZAMOWIENIA set
						POLE5 = @Part,
						POLE6 = cast(@idLokalizacji as varchar(100))
					where ID_POZYCJI_ZAMOWIENIA = @id_pozycji_zamowienia

					update POZYCJA_ZAMOWIENIA set
						POLE5 = @Part,
						POLE6 = cast(@idLokalizacji as varchar(100))
					where ID_POZYCJI_ZAMOWIENIA = @id_poz_zam_rozchod
				end

				delete from POZYCJA_ZAMOWIENIA where ID_POZYCJI_ZAMOWIENIA = @ID_POZYCJI_ZAMOWIENIA_stara

				FETCH NEXT FROM kursor_po_poz_rozpiski into @id, @id_poz_zam_przychod, @idLokalizacji, @Part, @Rozchod, @id_poz_zam_rozchod
			END

			CLOSE kursor_po_poz_rozpiski
			DEALLOCATE kursor_po_poz_rozpiski

		end

		FETCH NEXT FROM kursor_po_poz into @ID_POZYCJI_ZAMOWIENIA_stara, @ID_ARTYKULU, @KOD_VAT, @ZAMOWIONO,
			@ZREALIZOWANO, @ZAREZERWOWANO, @DO_REZERWACJI, @CENA_NETTO, @CENA_BRUTTO, @CENA_NETTO_WAL, 
			@CENA_BRUTTO_WAL, @PRZELICZNIK, @JEDNOSTKA, @NARZUT, @OPIS
	END

	CLOSE kursor_po_poz
	DEALLOCATE kursor_po_poz

	declare @suma_netto decimal(16, 4) 
	declare @suma_brutto decimal(16, 4)  
	declare @suma_netto_wal decimal(16, 4)  
	declare @suma_brutto_wal decimal(16, 4)  

	exec RM_SumujZamowienie_Server @idZamowienia, 
		@suma_netto output, @suma_brutto output, @suma_netto_wal output, @suma_brutto_wal output

	update ZAMOWIENIE set
		WARTOSC_BRUTTO = @suma_brutto,
		WARTOSC_NETTO = @suma_netto,
		WARTOSC_BRUTTO_WAL = @suma_brutto_wal,
		WARTOSC_NETTO_WAL = @suma_netto_wal
	where ID_ZAMOWIENIA = @idZamowienia


	declare @format_numeracji varchar(50)
	declare @okresnumeracji tinyint
	declare @parametr1 tinyint
	declare @parametr2 tinyint

	declare @id_typu int

	select top 1 @id_typu = id_typu from TYP_DOKUMENTU_MAGAZYNOWEGO where NAZWA = 'Zamówienie do dostawcy' and ID_FIRMY = @IdFirmy

--	exec JL_PobierzFormatNumeracji_Server @IdFirmy, 2, @id_typu, @IdMagazynu, 
--		@format_numeracji output, @okresnumeracji output, @parametr1 output, @parametr2 output

	exec RM_ZatwierdzZamowienie_Server @idZamowienia, @IdKontrahenta, @id_typu, @NUMER, @format_numeracji,
			@okresnumeracji, @parametr1, @parametr2, @autonumer, @IdFirmy, @IdMagazynu, @Data, @Data, 0, 0, 2, 
			'', 2, null,
			1, 0, '', 0, 1, 0, 0.00, 2, '', '', '', 0, '', 0

	if @@trancount>0 commit transaction
	goto Koniec 
	Error: 
		IF (SELECT CURSOR_STATUS('global','kursor_po_poz')) >= -1
		BEGIN
			CLOSE kursor_po_poz
			DEALLOCATE kursor_po_poz
		END

	IF (SELECT CURSOR_STATUS('global','kursor_po_poz_rozpiski')) >= -1
	BEGIN
		CLOSE kursor_po_poz_rozpiski
		DEALLOCATE kursor_po_poz_rozpiski
	END


	if @@trancount>0 rollback tran 
	goto Koniec 

	Koniec: 
		set transaction isolation level READ COMMITTED 
		return
	
end 
