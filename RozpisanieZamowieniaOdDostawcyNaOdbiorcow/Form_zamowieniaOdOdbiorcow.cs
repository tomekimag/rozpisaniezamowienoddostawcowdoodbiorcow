﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RozpisanieZamowieniaOdDostawcyNaOdbiorcow.Model;
using RozpisanieZamowieniaOdDostawcyNaOdbiorcow.Controllers;

namespace RozpisanieZamowieniaOdDostawcyNaOdbiorcow
{
    public partial class Form_zamowieniaOdOdbiorcow : Form
    {
        public List<ZamowienieModel> listaWybranychZamowien;

        public Form_zamowieniaOdOdbiorcow(List<ZamowienieModel> listaWybranychZamowien)
        {
            InitializeComponent();
            this.listaWybranychZamowien = listaWybranychZamowien;
            WyswietlListeWybranychZamowien();
        }

        private void WyswietlListeWybranychZamowien()
        {
            dataGridView_zamowieniaOdOdbiorcow.DataSource = null;
            dataGridView_zamowieniaOdOdbiorcow.DataSource = listaWybranychZamowien;
        }

        private void button_dodaj_Click(object sender, EventArgs e)
        {
            
            Form_zamowienia zamowienia = new Form_zamowienia();
            if (zamowienia.ShowDialog() == DialogResult.Yes)
            {
                dataGridView_zamowieniaOdOdbiorcow.DataSource = null;
                ZamowienieModel zamowienie = listaWybranychZamowien.Where(x => x.ID_ZAMOWIENIA == zamowienia.wybraneZamowienie.ID_ZAMOWIENIA).FirstOrDefault();

                if (zamowienie == null)
                {
                    dataGridView_zamowieniaOdOdbiorcow.DataSource = null;
                    listaWybranychZamowien.Add(zamowienia.wybraneZamowienie);

                    /*Tworzenie nowej listy jest próbą poradzenia sobie z błędem out of index exception.*/
                    List<ZamowienieModel> listaZamowien = new List<ZamowienieModel>();
                    listaZamowien.AddRange(listaWybranychZamowien);
                    listaWybranychZamowien = listaZamowien;

                    WyswietlListeWybranychZamowien();
                }
                else
                    MessageBox.Show("Zamówienie zostało już dodane do listy wybranych zamówień.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            
            //listaWybranychZamowien = ZamowienieController.PobierzWszystkieZamowieniaOdOdbiorcow();
            //WyswietlListeWybranychZamowien();

        }

        private void dataGridView_zamowieniaOdOdbiorcow_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "ID_ZAMOWIENIA":
                    e.Column.Visible = false;
                    break;
                case "ID_KONTRAHENTA":
                    e.Column.Visible = false;
                    break;
                case "ID_FIRMY":
                    e.Column.Visible = false;
                    break;
                case "ID_MAGAZYNU":
                    e.Column.Visible = false;
                    break;
                case "POLE1":
                    e.Column.Visible = false;
                    break;
                case "POLE2":
                    e.Column.Visible = false;
                    break;
                case "POLE3":
                    e.Column.Visible = false;
                    break;
                case "POLE4":
                    e.Column.Visible = false;
                    break;
                case "POLE6":
                    e.Column.Visible = false;
                    break;
                case "POLE7":
                    e.Column.Visible = false;
                    break;
                case "POLE8":
                    e.Column.Visible = false;
                    break;
                case "POLE9":
                    e.Column.Visible = false;
                    break;
                case "POLE10":
                    e.Column.Visible = false;
                    break;
                case "POLE5":
                    e.Column.Visible = false;
                    break;
                case "DATA":
                    e.Column.Visible = false;
                    break;
                default:
                    e.Column.Visible = true;
                    break;
            }
        }

        private void dataGridView_zamowieniaOdOdbiorcow_DataSourceChanged(object sender, EventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            dgv.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgv.MultiSelect = false;
            dgv.AutoResizeColumns();
            dgv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        private void button_zamknij_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button_usun_Click(object sender, EventArgs e)
        {
            if (dataGridView_zamowieniaOdOdbiorcow.SelectedRows == null)
            {
                MessageBox.Show("Wskarz pozycję.", "Imag Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (dataGridView_zamowieniaOdOdbiorcow.SelectedRows.Count != 0)
            {
                decimal idZamowienia = decimal.Parse(dataGridView_zamowieniaOdOdbiorcow["ID_ZAMOWIENIA", dataGridView_zamowieniaOdOdbiorcow.SelectedRows[0].Index].Value.ToString());

                ZamowienieModel zamowienie = listaWybranychZamowien.Where(x => x.ID_ZAMOWIENIA == idZamowienia).FirstOrDefault();

                if (zamowienie != null)
                {
                    listaWybranychZamowien.Remove(zamowienie);
                    List<ZamowienieModel> listaWybranychZamowienTmp = new List<ZamowienieModel>();
                    listaWybranychZamowienTmp.AddRange(listaWybranychZamowien);
                    listaWybranychZamowien = listaWybranychZamowienTmp;
                    WyswietlListeWybranychZamowien();
                }
            }
        }

        private void dataGridView_zamowieniaOdOdbiorcow_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show($@"twit check = {e.Exception.Message}");
        }
    }
}
