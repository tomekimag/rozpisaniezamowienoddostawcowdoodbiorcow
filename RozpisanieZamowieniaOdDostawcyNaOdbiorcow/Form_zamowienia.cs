﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RozpisanieZamowieniaOdDostawcyNaOdbiorcow.Model;
using RozpisanieZamowieniaOdDostawcyNaOdbiorcow.Controllers;

namespace RozpisanieZamowieniaOdDostawcyNaOdbiorcow
{
    public partial class Form_zamowienia : Form
    {
        public ZamowienieModel wybraneZamowienie;

        private List<ZamowienieModel> zamowienia;

        public Form_zamowienia()
        {
            InitializeComponent();
            PobierzWszystkieZamowieniaOdOdbiorcow();
            UstawPoleWyszukiwania();
        }

        private void UstawPoleWyszukiwania()
        {
            comboBox_pole.Text = "Kontrahent";
        }

        private void PobierzWszystkieZamowieniaOdOdbiorcow()
        {
            dataGridView_wszystkieZamOdOdbiorcow.DataSource = zamowienia = ZamowienieController.PobierzWszystkieZamowieniaOdOdbiorcow();
        }

        private void button_wyjdz_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void dataGridView_wszystkieZamOdOdbiorcow_DataSourceChanged(object sender, EventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            dgv.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgv.MultiSelect = false;
            dgv.AutoResizeColumns();
            dgv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        private void dataGridView_wszystkieZamOdOdbiorcow_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "ID_ZAMOWIENIA":
                    e.Column.Visible = false;
                    break;
                case "ID_KONTRAHENTA":
                    e.Column.Visible = false;
                    break;
                case "ID_FIRMY":
                    e.Column.Visible = false;
                    break;
                case "ID_MAGAZYNU":
                    e.Column.Visible = false;
                    break; 
                case "POLE1":
                    e.Column.Visible = false;
                    break;
                case "POLE2":
                    e.Column.Visible = false;
                    break;
                case "POLE3":
                    e.Column.Visible = false;
                    break;
                case "POLE4":
                    e.Column.Visible = false;
                    break;
                case "POLE6":
                    e.Column.Visible = false;
                    break;
                case "POLE7":
                    e.Column.Visible = false;
                    break;
                case "POLE8":
                    e.Column.Visible = false;
                    break;
                case "POLE9":
                    e.Column.Visible = false;
                    break;
                case "POLE10":
                    e.Column.Visible = false;
                    break;
                case "POLE5":
                    e.Column.Visible = false;
                    break;
                case "DATA":
                    e.Column.Visible = false;
                    break; 
                default:
                    e.Column.Visible = true;
                    break;
            }
        }

        private void button_wybierz_Click(object sender, EventArgs e)
        {
            if (dataGridView_wszystkieZamOdOdbiorcow.SelectedRows == null)
            {
                MessageBox.Show("Wskarz zamówienie.", "Imag Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (dataGridView_wszystkieZamOdOdbiorcow.SelectedRows.Count != 0)
            {
                decimal id_zamowienia = decimal.Parse(dataGridView_wszystkieZamOdOdbiorcow["ID_ZAMOWIENIA", dataGridView_wszystkieZamOdOdbiorcow.SelectedRows[0].Index].Value.ToString());

                wybraneZamowienie = zamowienia.Where(x => x.ID_ZAMOWIENIA == id_zamowienia).FirstOrDefault();

                if (wybraneZamowienie != null)
                {
                    this.DialogResult = DialogResult.Yes;
                    Close();
                }
            }
        }

        private void button_wszystko_Click(object sender, EventArgs e)
        {
            textBox_szukanyCiag.Text = "";
            dataGridView_wszystkieZamOdOdbiorcow.DataSource = zamowienia;
        }

        private void button_szukaj_Click(object sender, EventArgs e)
        {
            List<ZamowienieModel> zamowieniaTmp;

            if (comboBox_pole.Text == string.Empty) {
                MessageBox.Show("Wybierz pole po którym chcesz szukać.", "Imag Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (comboBox_pole.Text == "Kontrahent")
            {
                zamowieniaTmp = zamowienia.Where(x => x.KONTRAHENT_NAZWA.ToUpper().Contains(textBox_szukanyCiag.Text.ToUpper())).ToList();
                dataGridView_wszystkieZamOdOdbiorcow.DataSource = zamowieniaTmp;
            }
            else if (comboBox_pole.Text == "Numer")
            {
                zamowieniaTmp = zamowienia.Where(x => x.NUMER.ToUpper().Contains(textBox_szukanyCiag.Text.ToUpper())).ToList();
                dataGridView_wszystkieZamOdOdbiorcow.DataSource = zamowieniaTmp;
            }
            else if (comboBox_pole.Text == "NR_ZAMOWIENIA_KLIENTA")
            {
                zamowieniaTmp = zamowienia.Where(x => (x.NR_ZAMOWIENIA_KLIENTA??"").ToUpper().Contains(textBox_szukanyCiag.Text.ToUpper())).ToList();
                dataGridView_wszystkieZamOdOdbiorcow.DataSource = zamowieniaTmp;
            }
        }
    }
}
