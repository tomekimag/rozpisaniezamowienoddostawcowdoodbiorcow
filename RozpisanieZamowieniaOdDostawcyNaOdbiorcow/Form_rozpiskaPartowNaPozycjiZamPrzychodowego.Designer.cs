﻿namespace RozpisanieZamowieniaOdDostawcyNaOdbiorcow
{
    partial class Form_rozpiskaPartowNaPozycjiZamPrzychodowego
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.button_zmienPART = new System.Windows.Forms.Button();
            this.button_zamknij = new System.Windows.Forms.Button();
            this.dataGridView_rozpiskaPartow = new System.Windows.Forms.DataGridView();
            this.numericUpDown_rozpisanoSuma = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_zamowionoSuma = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button_eksportDoExcela = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_rozpiskaPartow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_rozpisanoSuma)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_zamowionoSuma)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button_eksportDoExcela);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.numericUpDown_zamowionoSuma);
            this.panel1.Controls.Add(this.numericUpDown_rozpisanoSuma);
            this.panel1.Controls.Add(this.button_zmienPART);
            this.panel1.Controls.Add(this.button_zamknij);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 434);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(802, 81);
            this.panel1.TabIndex = 0;
            // 
            // button_zmienPART
            // 
            this.button_zmienPART.Location = new System.Drawing.Point(32, 30);
            this.button_zmienPART.Name = "button_zmienPART";
            this.button_zmienPART.Size = new System.Drawing.Size(107, 23);
            this.button_zmienPART.TabIndex = 1;
            this.button_zmienPART.Text = "Zmień PART";
            this.button_zmienPART.UseVisualStyleBackColor = true;
            this.button_zmienPART.Click += new System.EventHandler(this.button_edytuj_Click);
            // 
            // button_zamknij
            // 
            this.button_zamknij.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_zamknij.Location = new System.Drawing.Point(694, 30);
            this.button_zamknij.Name = "button_zamknij";
            this.button_zamknij.Size = new System.Drawing.Size(75, 23);
            this.button_zamknij.TabIndex = 0;
            this.button_zamknij.Text = "Zamknij";
            this.button_zamknij.UseVisualStyleBackColor = true;
            this.button_zamknij.Click += new System.EventHandler(this.button_zamknij_Click);
            // 
            // dataGridView_rozpiskaPartow
            // 
            this.dataGridView_rozpiskaPartow.AllowUserToAddRows = false;
            this.dataGridView_rozpiskaPartow.AllowUserToDeleteRows = false;
            this.dataGridView_rozpiskaPartow.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_rozpiskaPartow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView_rozpiskaPartow.Location = new System.Drawing.Point(0, 0);
            this.dataGridView_rozpiskaPartow.Name = "dataGridView_rozpiskaPartow";
            this.dataGridView_rozpiskaPartow.ReadOnly = true;
            this.dataGridView_rozpiskaPartow.Size = new System.Drawing.Size(802, 434);
            this.dataGridView_rozpiskaPartow.TabIndex = 1;
            this.dataGridView_rozpiskaPartow.DataSourceChanged += new System.EventHandler(this.dataGridView_rozpiskaPartow_DataSourceChanged);
            this.dataGridView_rozpiskaPartow.ColumnAdded += new System.Windows.Forms.DataGridViewColumnEventHandler(this.dataGridView_rozpiskaPartow_ColumnAdded);
            // 
            // numericUpDown_rozpisanoSuma
            // 
            this.numericUpDown_rozpisanoSuma.DecimalPlaces = 4;
            this.numericUpDown_rozpisanoSuma.Enabled = false;
            this.numericUpDown_rozpisanoSuma.Location = new System.Drawing.Point(189, 33);
            this.numericUpDown_rozpisanoSuma.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.numericUpDown_rozpisanoSuma.Name = "numericUpDown_rozpisanoSuma";
            this.numericUpDown_rozpisanoSuma.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown_rozpisanoSuma.TabIndex = 2;
            // 
            // numericUpDown_zamowionoSuma
            // 
            this.numericUpDown_zamowionoSuma.DecimalPlaces = 4;
            this.numericUpDown_zamowionoSuma.Enabled = false;
            this.numericUpDown_zamowionoSuma.Location = new System.Drawing.Point(315, 33);
            this.numericUpDown_zamowionoSuma.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.numericUpDown_zamowionoSuma.Name = "numericUpDown_zamowionoSuma";
            this.numericUpDown_zamowionoSuma.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown_zamowionoSuma.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(186, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Suma rozpisanych:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(312, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Suma zamówionych:";
            // 
            // button_eksportDoExcela
            // 
            this.button_eksportDoExcela.Location = new System.Drawing.Point(467, 30);
            this.button_eksportDoExcela.Name = "button_eksportDoExcela";
            this.button_eksportDoExcela.Size = new System.Drawing.Size(132, 23);
            this.button_eksportDoExcela.TabIndex = 6;
            this.button_eksportDoExcela.Text = "Eksport do EXCEL";
            this.button_eksportDoExcela.UseVisualStyleBackColor = true;
            this.button_eksportDoExcela.Click += new System.EventHandler(this.button_eksportDoExcela_Click);
            // 
            // Form_rozpiskaPartowNaPozycjiZamPrzychodowego
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(802, 515);
            this.Controls.Add(this.dataGridView_rozpiskaPartow);
            this.Controls.Add(this.panel1);
            this.Name = "Form_rozpiskaPartowNaPozycjiZamPrzychodowego";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Rozpiska partów";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_rozpiskaPartow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_rozpisanoSuma)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_zamowionoSuma)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dataGridView_rozpiskaPartow;
        private System.Windows.Forms.Button button_zamknij;
        private System.Windows.Forms.Button button_zmienPART;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericUpDown_zamowionoSuma;
        private System.Windows.Forms.NumericUpDown numericUpDown_rozpisanoSuma;
        private System.Windows.Forms.Button button_eksportDoExcela;
    }
}