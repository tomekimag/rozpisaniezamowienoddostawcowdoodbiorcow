﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RozpisanieZamowieniaOdDostawcyNaOdbiorcow.Utils
{
    public class DateHelper
    {
        public static DateTime InTtoDate(int data)
        {
            if (data <= 36163) return new DateTime();

            try
            {
                var d = data - 36163;
                var d2 = new DateTime(1900, 1, 1);
                d2 = d2.AddDays(d);

                return d2;
            }
            catch (Exception ex)
            {
                return new DateTime();
            }
        }
        public static int DateToInt(string dataString)
        {
            try
            {
                DateTime data = DateTime.Parse(dataString);
                var start = new DateTime(1900, 1, 1);
                var dif = (data - start).TotalDays;
                dif = dif + 36163;
                return Convert.ToInt32(dif);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public static int DateToInt(DateTime data)
        {
            try
            {
                var start = new DateTime(1900, 1, 1);
                var dif = (data - start).TotalDays;
                dif = dif + 36163;
                return Convert.ToInt32(dif);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public static string DateTimeToStringLongFormat(DateTime date)
        {
            string month = date.Month < 10 ? "0" : "";
            month = month + date.Month.ToString();
            string day = date.Day < 10 ? "0" : "";
            day = day + date.Day.ToString();
            string year = date.Year.ToString();
            if (year.Length == 1) year = $@"000{year}";
            else if (year.Length == 2) year = $@"00{year}";
            else if (year.Length == 3) year = $@"0{year}";
            string sDate = $@"{year}-{month}-{day}T00:00:00";
            return sDate;
        }

        public static string DateTimeToStringShortFormat(DateTime date)
        {
            string month = date.Month < 10 ? "0" : "";
            month = month + date.Month.ToString();
            string day = date.Day < 10 ? "0" : "";
            day = day + date.Day.ToString();
            string year = date.Year.ToString();
            if (year.Length == 1) year = $@"000{year}";
            else if (year.Length == 2) year = $@"00{year}";
            else if (year.Length == 3) year = $@"0{year}";
            string sDate = $@"{year}-{month}-{day}";
            return sDate;
        }
    }
}
