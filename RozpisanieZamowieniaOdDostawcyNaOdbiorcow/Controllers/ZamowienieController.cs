﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RozpisanieZamowieniaOdDostawcyNaOdbiorcow.Model;
using RozpisanieZamowieniaOdDostawcyNaOdbiorcow.Utils;

namespace RozpisanieZamowieniaOdDostawcyNaOdbiorcow.Controllers
{
    public static class ZamowienieController
    {
        public static ZamowienieModel PobierzZamowienie()
        {
            ZamowienieModel zamowienie;

            using (NOWASA2018Entities entity = new NOWASA2018Entities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                zamowienie = (from z in entity.ZAMOWIENIE
                              where z.ID_ZAMOWIENIA == ParametryUruchomienioweController.idZamowienia
                              select new ZamowienieModel()
                              {
                                  ID_ZAMOWIENIA = z.ID_ZAMOWIENIA,
                                  DATA = z.DATA,
                                  ID_FIRMY = z.ID_FIRMY,
                                  ID_KONTRAHENTA = z.ID_KONTRAHENTA,
                                  ID_MAGAZYNU = z.ID_MAGAZYNU,
                                  KONTRAHENT_NAZWA = z.KONTRAHENT_NAZWA,
                                  NUMER = z.NUMER,
                                  POLE1 = z.POLE1,
                                  POLE2 = z.POLE2,
                                  POLE3 = z.POLE3,
                                  POLE4 = z.POLE4,
                                  POLE5 = z.POLE5,
                                  POLE6 = z.POLE6,
                                  POLE7 = z.POLE7,
                                  POLE8 = z.POLE8,
                                  POLE9 = z.POLE9,
                                  POLE10 = z.POLE10
                              }).FirstOrDefault();
            }

            return zamowienie;
        }

        public static List<ZamowienieModel> PobierzWszystkieZamowieniaOdOdbiorcow()
        {
            List<ZamowienieModel> listaZamowien;

            using (NOWASA2018Entities entity = new NOWASA2018Entities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                listaZamowien = (from z in entity.ZAMOWIENIE
                              where z.TYP == 1
                              orderby z.ID_ZAMOWIENIA descending
                              select new ZamowienieModel()
                              {
                                  ID_ZAMOWIENIA = z.ID_ZAMOWIENIA,
                                  DATA = z.DATA,
                                  ID_FIRMY = z.ID_FIRMY,
                                  ID_KONTRAHENTA = z.ID_KONTRAHENTA,
                                  ID_MAGAZYNU = z.ID_MAGAZYNU,
                                  KONTRAHENT_NAZWA = z.KONTRAHENT_NAZWA,
                                  NUMER = z.NUMER,
                                  POLE1 = z.POLE1,
                                  POLE2 = z.POLE2,
                                  POLE3 = z.POLE3,
                                  POLE4 = z.POLE4,
                                  POLE5 = z.POLE5,
                                  POLE6 = z.POLE6,
                                  POLE7 = z.POLE7,
                                  POLE8 = z.POLE8,
                                  POLE9 = z.POLE9,
                                  POLE10 = z.POLE10,
                                  NR_ZAMOWIENIA_KLIENTA = z.NR_ZAMOWIENIA_KLIENTA
                              }).ToList();
            }

            foreach(ZamowienieModel zamowienie in listaZamowien)
            {
                zamowienie.DATA_ZAMOWIENIA = DateHelper.InTtoDate(zamowienie.DATA??0);
            }

            return listaZamowien;
        }

        public static ZamowienieModel PobierzZamowieniePoNumerze(string numer)
        {
            ZamowienieModel zamowienie;

            using (NOWASA2018Entities entity = new NOWASA2018Entities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                zamowienie = (from z in entity.ZAMOWIENIE
                              where z.ID_MAGAZYNU == ParametryUruchomienioweController.idMagazynu && z.NUMER == numer
                              select new ZamowienieModel()
                              {
                                  ID_ZAMOWIENIA = z.ID_ZAMOWIENIA,
                                  DATA = z.DATA,
                                  ID_FIRMY = z.ID_FIRMY,
                                  ID_KONTRAHENTA = z.ID_KONTRAHENTA,
                                  ID_MAGAZYNU = z.ID_MAGAZYNU,
                                  KONTRAHENT_NAZWA = z.KONTRAHENT_NAZWA,
                                  NUMER = z.NUMER,
                                  POLE1 = z.POLE1,
                                  POLE2 = z.POLE2,
                                  POLE3 = z.POLE3,
                                  POLE4 = z.POLE4,
                                  POLE5 = z.POLE5,
                                  POLE6 = z.POLE6,
                                  POLE7 = z.POLE7,
                                  POLE8 = z.POLE8,
                                  POLE9 = z.POLE9,
                                  POLE10 = z.POLE10
                              }).FirstOrDefault();
            }

            return zamowienie;
        }

        public static ZamowienieModel PobierzZamowieniePoIdPozycjiZamowienia(decimal idPozycjiZamowienia)
        {
            ZamowienieModel zamowienie;

            using (NOWASA2018Entities entity = new NOWASA2018Entities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                zamowienie = (from z in entity.ZAMOWIENIE
                              join pz in entity.POZYCJA_ZAMOWIENIA on z.ID_ZAMOWIENIA equals pz.ID_ZAMOWIENIA
                              where pz.ID_POZYCJI_ZAMOWIENIA == idPozycjiZamowienia
                              select new ZamowienieModel()
                              {
                                  ID_ZAMOWIENIA = z.ID_ZAMOWIENIA,
                                  DATA = z.DATA,
                                  ID_FIRMY = z.ID_FIRMY,
                                  ID_KONTRAHENTA = z.ID_KONTRAHENTA,
                                  ID_MAGAZYNU = z.ID_MAGAZYNU,
                                  KONTRAHENT_NAZWA = z.KONTRAHENT_NAZWA,
                                  NUMER = z.NUMER,
                                  POLE1 = z.POLE1,
                                  POLE2 = z.POLE2,
                                  POLE3 = z.POLE3,
                                  POLE4 = z.POLE4,
                                  POLE5 = z.POLE5,
                                  POLE6 = z.POLE6,
                                  POLE7 = z.POLE7,
                                  POLE8 = z.POLE8,
                                  POLE9 = z.POLE9,
                                  POLE10 = z.POLE10
                              }).FirstOrDefault();
            }

            return zamowienie;
        }

        public static void PrzetworzZamowieniePrzychodoweNaPartachILokalizacjach()
        {
            using (NOWASA2018Entities entity = new NOWASA2018Entities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                entity.IMAG_twit_przetworz_zamowienia_do_dostawcow_wg_partow_i_lokalizacji(
                    ParametryUruchomienioweController.idFirmy, ParametryUruchomienioweController.idMagazynu, ParametryUruchomienioweController.idUzytkownika, ParametryUruchomienioweController.idZamowienia
                );
            }
        }

        public static List<ZamowienieModel> PobierzWszystkieZamowieniaZeStatusemOczekujaceNaPlatnosc()
        {
            List<ZamowienieModel> listaZamowien;

            using (NOWASA2018Entities entity = new NOWASA2018Entities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                listaZamowien = (from z in entity.ZAMOWIENIE
                                 where z.TYP == 1
                                 && z.STATUS_ZAM == "O"
                                 orderby z.ID_ZAMOWIENIA descending
                                 select new ZamowienieModel()
                                 {
                                     ID_ZAMOWIENIA = z.ID_ZAMOWIENIA,
                                     DATA = z.DATA,
                                     ID_FIRMY = z.ID_FIRMY,
                                     ID_KONTRAHENTA = z.ID_KONTRAHENTA,
                                     ID_MAGAZYNU = z.ID_MAGAZYNU,
                                     KONTRAHENT_NAZWA = z.KONTRAHENT_NAZWA,
                                     NUMER = z.NUMER,
                                     POLE1 = z.POLE1,
                                     POLE2 = z.POLE2,
                                     POLE3 = z.POLE3,
                                     POLE4 = z.POLE4,
                                     POLE5 = z.POLE5,
                                     POLE6 = z.POLE6,
                                     POLE7 = z.POLE7,
                                     POLE8 = z.POLE8,
                                     POLE9 = z.POLE9,
                                     POLE10 = z.POLE10,
                                     NR_ZAMOWIENIA_KLIENTA = z.NR_ZAMOWIENIA_KLIENTA
                                 }).ToList();
            }

            foreach (ZamowienieModel zamowienie in listaZamowien)
            {
                zamowienie.DATA_ZAMOWIENIA = DateHelper.InTtoDate(zamowienie.DATA ?? 0);
            }

            return listaZamowien;
        }
    }
}
