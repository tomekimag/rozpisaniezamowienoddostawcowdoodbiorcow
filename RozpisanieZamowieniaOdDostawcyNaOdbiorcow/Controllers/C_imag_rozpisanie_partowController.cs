﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RozpisanieZamowieniaOdDostawcyNaOdbiorcow.Model;
using RozpisanieZamowieniaOdDostawcyNaOdbiorcow.Utils;
using System.Windows.Forms;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace RozpisanieZamowieniaOdDostawcyNaOdbiorcow.Controllers
{
    public static class C_imag_rozpisanie_partowController
    {
        public static void RozpiszLokalizacjePartow(decimal id_poz_zam_przych, string Part, long idLokalizacjiWMS, decimal ilosc, string PartPierwotny)
        {
            using (NOWASA2018Entities entity = new NOWASA2018Entities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                entity.C_imag_rozpisanie_partow.Add(new C_imag_rozpisanie_partow() {
                    idLokalizacji = idLokalizacjiWMS,
                    id_poz_zam_przychod = id_poz_zam_przych,
                    id_poz_zam_rozchod = null,
                    id_uzytkownika = ParametryUruchomienioweController.idUzytkownika,
                    Part = Part,
                    Rozchod = ilosc,
                    PartPierwotny = PartPierwotny
                });

                entity.SaveChanges();
            }
        }

        public static void RozpiszLokalizacjePartow(decimal id_poz_zam_przych, string Part, decimal idLokalizacjiWMS, decimal ilosc, decimal id_poz_zam_rozchodowych, string PartPierwotny)
        {
            try
            {
                using (NOWASA2018Entities entity = new NOWASA2018Entities(ParametryUruchomienioweController.connectionStringToModelEntities))
                {
                    entity.C_imag_rozpisanie_partow.Add(new C_imag_rozpisanie_partow()
                    {
                        idLokalizacji = idLokalizacjiWMS,
                        id_poz_zam_przychod = id_poz_zam_przych,
                        id_poz_zam_rozchod = id_poz_zam_rozchodowych,
                        id_uzytkownika = ParametryUruchomienioweController.idUzytkownika,
                        Part = Part,
                        Rozchod = ilosc,
                        PartPierwotny = PartPierwotny
                    });

                    entity.SaveChanges();
                }
            }
            catch (Exception e)
            {
                if (e.InnerException != null)
                    MessageBox.Show($@"{e.Message}{e.InnerException.Message}", "Pytanie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                    MessageBox.Show($@"{e.Message}", "Pytanie", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
        }

        public static void WyczyscTabelkeRozpisuPartow()
        {
            try
            {
                using (NOWASA2018Entities entity = new NOWASA2018Entities(ParametryUruchomienioweController.connectionStringToModelEntities))
                {
                    var listaDanychDoUsuniecia = from rp in entity.C_imag_rozpisanie_partow
                                                 where rp.id_uzytkownika == ParametryUruchomienioweController.idUzytkownika
                                                 && rp.historyczny == 0
                                                 select rp;

                    if (listaDanychDoUsuniecia.Count() != 0)
                    {
                        entity.C_imag_rozpisanie_partow.RemoveRange(listaDanychDoUsuniecia);

                        entity.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                if(e.InnerException != null)
                    MessageBox.Show($@"{e.Message}{e.InnerException.Message}", "Pytanie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                    MessageBox.Show($@"{e.Message}", "Pytanie", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
        }


        public static void HistoryzujZapisyTabelkiRozpisuPartow()
        {
            using (NOWASA2018Entities entity = new NOWASA2018Entities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                var listaDanychDoHistoryzacji = from rp in entity.C_imag_rozpisanie_partow
                                                where rp.id_uzytkownika == ParametryUruchomienioweController.idUzytkownika
                                                && rp.historyczny == 0
                                                select rp;

                foreach (var h in listaDanychDoHistoryzacji)
                {
                    h.historyczny = 1;
                }

                entity.SaveChanges();
            }
        }

        public static List<RozpiskaPartowModel> ZwrocRozpiskeZDanymiZPozycjiDlaPart1lub2_poPozycjiPrzychodowej(decimal id_poz_zam_przychodowego)
        {
            List<RozpiskaPartowModel> rozpiski;

            using (NOWASA2018Entities entity = new NOWASA2018Entities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                rozpiski = (from rp in entity.C_imag_rozpisanie_partow
                            where rp.id_poz_zam_przychod == id_poz_zam_przychodowego && rp.id_uzytkownika == ParametryUruchomienioweController.idUzytkownika
                            select new RozpiskaPartowModel()
                            {
                                id = rp.id,
                                id_poz_zam_rozchod = rp.id_poz_zam_rozchod,
                                id_poz_zam_przychod = rp.id_poz_zam_przychod,
                                idLokalizacji = rp.idLokalizacji,
                                id_uzytkownika = rp.id_uzytkownika,
                                KONTRAHENT_NAZWA = "",
                                NUMER = "",
                                Part = rp.Part,
                                Rozchod = rp.Rozchod,
                                ZAMOWIONO = 0
                            }).ToList();

                foreach(var rozpiska in rozpiski)
                {
                    if (rozpiska.id_poz_zam_rozchod != null)
                    {
                        var dane = (from pzr in entity.POZYCJA_ZAMOWIENIA
                                    join z in entity.ZAMOWIENIE on pzr.ID_ZAMOWIENIA equals z.ID_ZAMOWIENIA
                                    where pzr.ID_POZYCJI_ZAMOWIENIA == rozpiska.id_poz_zam_rozchod
                                    select new { NUMER = z.NUMER, KONTRAHENT_NAZWA = z.KONTRAHENT_NAZWA, ZAMOWIONO = pzr.ZAMOWIONO }).FirstOrDefault();

                        if (dane != null)
                        {
                            rozpiska.NUMER = dane.NUMER;
                            rozpiska.KONTRAHENT_NAZWA = dane.KONTRAHENT_NAZWA;
                            rozpiska.ZAMOWIONO = dane.ZAMOWIONO;
                        }
                    }
                }

                return rozpiski;
            }
        }

        public static void ZamienLokalizacjeIPartWRozpisce(long idRozpiski, vLokalizacjaModel wybranyPart)
        {
            using (NOWASA2018Entities entity = new NOWASA2018Entities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                C_imag_rozpisanie_partow rozpiska = (from rp in entity.C_imag_rozpisanie_partow
                                                     where rp.id == idRozpiski
                                                     select rp).FirstOrDefault();

                rozpiska.idLokalizacji = (decimal)wybranyPart.idLokalizacji;
                rozpiska.Part = wybranyPart.lokalizacja;

                entity.SaveChanges();
            }
        }

        public static List<RozpiskaPartowModel> ZwrocRozpiskeZDanymiZPozycjiDlaPart1lub2_poPozycjiRozchodowej(decimal id_poz_zam_rozchodowej)
        {
            List<RozpiskaPartowModel> rozpiski;

            using (NOWASA2018Entities entity = new NOWASA2018Entities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                rozpiski = (from rp in entity.C_imag_rozpisanie_partow
                            where rp.id_poz_zam_rozchod == id_poz_zam_rozchodowej && rp.id_uzytkownika == ParametryUruchomienioweController.idUzytkownika
                            select new RozpiskaPartowModel()
                            {
                                id = rp.id,
                                id_poz_zam_rozchod = rp.id_poz_zam_rozchod,
                                id_poz_zam_przychod = rp.id_poz_zam_przychod,
                                idLokalizacji = rp.idLokalizacji,
                                id_uzytkownika = rp.id_uzytkownika,
                                KONTRAHENT_NAZWA = "",
                                NUMER = "",
                                Part = rp.Part,
                                Rozchod = rp.Rozchod,
                                ZAMOWIONO = 0
                            }).ToList();

                foreach (var rozpiska in rozpiski)
                {
                    if (rozpiska.id_poz_zam_rozchod != null)
                    {
                        var dane = (from pzr in entity.POZYCJA_ZAMOWIENIA
                                    join z in entity.ZAMOWIENIE on pzr.ID_ZAMOWIENIA equals z.ID_ZAMOWIENIA
                                    where pzr.ID_POZYCJI_ZAMOWIENIA == rozpiska.id_poz_zam_rozchod
                                    select new { NUMER = z.NUMER, KONTRAHENT_NAZWA = z.KONTRAHENT_NAZWA, ZAMOWIONO = pzr.ZAMOWIONO }).FirstOrDefault();

                        if (dane != null)
                        {
                            rozpiska.NUMER = dane.NUMER;
                            rozpiska.KONTRAHENT_NAZWA = dane.KONTRAHENT_NAZWA;
                            rozpiska.ZAMOWIONO = dane.ZAMOWIONO;
                        }
                    }
                }

                return rozpiski;
            }
        }

        public static void ZapiszRozpiskeDoPlikuExcel(List<RozpiskaPartowModel> listaRozpiski, bool pelnaRozpiska = false)
        {
            SaveFileDialog zapisPliku = new SaveFileDialog();
            zapisPliku.Filter = "Excel plik|*.xlsx";
            zapisPliku.Title = "Zapis pliku excel";
            zapisPliku.ShowDialog();

            if (zapisPliku.FileName != "")
            {
                try
                {
                    using (FileStream stream = new FileStream(zapisPliku.FileName, FileMode.Create, FileAccess.Write))
                    {
                        IWorkbook wb = new XSSFWorkbook();
                        ISheet sheet = wb.CreateSheet("Rozpis podziału PART");
                        ICreationHelper cH = wb.GetCreationHelper();

                        IRow row = sheet.CreateRow(0);
                        ICell cell;

                        cell = row.CreateCell(0);
                        cell.SetCellValue(cH.CreateRichTextString("idLokalizacji"));

                        cell = row.CreateCell(1);
                        cell.SetCellValue(cH.CreateRichTextString("Part"));

                        cell = row.CreateCell(2);
                        cell.SetCellValue(cH.CreateRichTextString("Rozchód"));

                        cell = row.CreateCell(3);
                        cell.SetCellValue(cH.CreateRichTextString("ZAMÓWIONO"));

                        cell = row.CreateCell(4);
                        cell.SetCellValue(cH.CreateRichTextString("NUMER"));

                        cell = row.CreateCell(5);
                        cell.SetCellValue(cH.CreateRichTextString("KONTRAHENT NAZWA"));

                        if (pelnaRozpiska)
                        {
                            cell = row.CreateCell(6);
                            cell.SetCellValue(cH.CreateRichTextString("NAZWA"));

                            cell = row.CreateCell(7);
                            cell.SetCellValue(cH.CreateRichTextString("INDEKS_KATALOGOWY"));
                        }


                        int iRow = 1;
                        foreach (RozpiskaPartowModel pozycjaRozpiski in listaRozpiski)
                        {
                            row = sheet.CreateRow(iRow);

                            cell = row.CreateCell(0);
                            cell.SetCellValue(cH.CreateRichTextString(pozycjaRozpiski.idLokalizacji.ToString()));

                            cell = row.CreateCell(1);
                            cell.SetCellValue(cH.CreateRichTextString(pozycjaRozpiski.Part.ToString()));

                            cell = row.CreateCell(2);
                            cell.SetCellValue(cH.CreateRichTextString(pozycjaRozpiski.Rozchod.ToString()));

                            cell = row.CreateCell(3);
                            cell.SetCellValue(cH.CreateRichTextString(pozycjaRozpiski.ZAMOWIONO.ToString()));

                            cell = row.CreateCell(4);
                            cell.SetCellValue(cH.CreateRichTextString(pozycjaRozpiski.NUMER.ToString()));

                            cell = row.CreateCell(5);
                            cell.SetCellValue(cH.CreateRichTextString(pozycjaRozpiski.KONTRAHENT_NAZWA.ToString()));

                            if (pelnaRozpiska)
                            {
                                cell = row.CreateCell(6);
                                cell.SetCellValue(cH.CreateRichTextString(pozycjaRozpiski.NAZWA.ToString()));

                                cell = row.CreateCell(7);
                                cell.SetCellValue(cH.CreateRichTextString(pozycjaRozpiski.INDEKS_KATALOGOWY.ToString()));
                            }

                            iRow++;
                        }

                        wb.Write(stream);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show($@"Zapis się nie powiódł. {ex.Message}", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        public static List<RozpiskaPartowModel> ZwrocRozpiskeZDanymiZPozycjiDlaWszystkichPartow()
        {
            List<RozpiskaPartowModel> rozpiski;

            using (NOWASA2018Entities entity = new NOWASA2018Entities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                rozpiski = (from rp in entity.C_imag_rozpisanie_partow
                            join pz in entity.POZYCJA_ZAMOWIENIA on rp.id_poz_zam_przychod equals pz.ID_POZYCJI_ZAMOWIENIA
                            join a in entity.ARTYKUL on pz.ID_ARTYKULU equals a.ID_ARTYKULU
                            where rp.id_uzytkownika == ParametryUruchomienioweController.idUzytkownika
                            select new RozpiskaPartowModel()
                            {
                                id = rp.id,
                                id_poz_zam_rozchod = rp.id_poz_zam_rozchod,
                                id_poz_zam_przychod = rp.id_poz_zam_przychod,
                                idLokalizacji = rp.idLokalizacji,
                                id_uzytkownika = rp.id_uzytkownika,
                                KONTRAHENT_NAZWA = "",
                                NUMER = "",
                                Part = rp.Part,
                                Rozchod = rp.Rozchod,
                                ZAMOWIONO = 0,
                                NAZWA = a.NAZWA,
                                INDEKS_KATALOGOWY = a.INDEKS_KATALOGOWY
                            }).ToList();

                foreach (var rozpiska in rozpiski)
                {
                    if (rozpiska.id_poz_zam_rozchod != null)
                    {
                        var dane = (from pzr in entity.POZYCJA_ZAMOWIENIA
                                    join z in entity.ZAMOWIENIE on pzr.ID_ZAMOWIENIA equals z.ID_ZAMOWIENIA
                                    where pzr.ID_POZYCJI_ZAMOWIENIA == rozpiska.id_poz_zam_rozchod
                                    select new { NUMER = z.NUMER, KONTRAHENT_NAZWA = z.KONTRAHENT_NAZWA, ZAMOWIONO = pzr.ZAMOWIONO }).FirstOrDefault();

                        if (dane != null)
                        {
                            rozpiska.NUMER = dane.NUMER;
                            rozpiska.KONTRAHENT_NAZWA = dane.KONTRAHENT_NAZWA;
                            rozpiska.ZAMOWIONO = dane.ZAMOWIONO;
                        }
                    }
                }

                return rozpiski;
            }
        }
    }
}
