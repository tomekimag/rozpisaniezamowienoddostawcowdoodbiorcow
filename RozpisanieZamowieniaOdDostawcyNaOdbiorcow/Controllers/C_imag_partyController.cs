﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RozpisanieZamowieniaOdDostawcyNaOdbiorcow.Model;

namespace RozpisanieZamowieniaOdDostawcyNaOdbiorcow.Controllers
{
    /// <summary>
    /// Klasa nieużywana. Do wycięcia.
    /// </summary>
    public static class C_imag_partyController
    {

   

        public static decimal PobranieIlosciJuzRozpisanejWPartachInnychNiz1i2(decimal idKontrahenta, decimal idArtykulu)
        {
            decimal wynik = 0;

            List<decimal> wyniki;

            using (NOWASA2018Entities entity = new NOWASA2018Entities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                wyniki = (from l in entity.vLokalizacje
                          join r in entity.C_imag_rozpisanie_partow on l.lokalizacja equals r.Part
                          join pzp in entity.POZYCJA_ZAMOWIENIA on r.id_poz_zam_przychod equals pzp.ID_POZYCJI_ZAMOWIENIA
                          join pzr in entity.POZYCJA_ZAMOWIENIA on r.id_poz_zam_rozchod equals pzr.ID_POZYCJI_ZAMOWIENIA
                          join zr in entity.ZAMOWIENIE on pzr.ID_ZAMOWIENIA equals zr.ID_ZAMOWIENIA
                          where zr.STATUS_ZAM == "O"
                          && l.idKontrahenta == idKontrahenta
                          && ((r.id_uzytkownika == ParametryUruchomienioweController.idUzytkownika && r.historyczny == 0) || r.historyczny == 1)
                          && pzp.ID_ARTYKULU == idArtykulu
                          && ((r.Part.ToUpper().Trim() != "PART1" && r.Part.ToUpper().Trim() != "PART2" && r.historyczny == 0)
                            || (r.PartPierwotny.ToUpper().Trim() != "PART1" && r.PartPierwotny.ToUpper().Trim() != "PART2" && r.historyczny == 1))
                          select r.Rozchod).ToList();
            }

            foreach (var w in wyniki) { wynik = wynik + w; }

            return wynik;
            
        }

        public static decimal PobranieIlosciJuzRozpisanejZPartu1i2NaPozycjeZHistorii(decimal idKontrahenta, decimal idPozycjiZamowieniaRozchodowej)
        {
            decimal wynik = 0;

            List<decimal> wyniki;

            using (NOWASA2018Entities entity = new NOWASA2018Entities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                wyniki = (from l in entity.vLokalizacje
                          join r in entity.C_imag_rozpisanie_partow on l.lokalizacja equals r.Part
                          //join pzp in entity.POZYCJA_ZAMOWIENIA on r.id_poz_zam_przychod equals pzp.ID_POZYCJI_ZAMOWIENIA
                          join pzr in entity.POZYCJA_ZAMOWIENIA on r.id_poz_zam_rozchod equals pzr.ID_POZYCJI_ZAMOWIENIA
                          join zr in entity.ZAMOWIENIE on pzr.ID_ZAMOWIENIA equals zr.ID_ZAMOWIENIA
                          where zr.STATUS_ZAM == "O"
                          && l.idKontrahenta == idKontrahenta
                          && r.id_uzytkownika == ParametryUruchomienioweController.idUzytkownika
                          && pzr.ID_POZYCJI_ZAMOWIENIA == idPozycjiZamowieniaRozchodowej
                          && (r.PartPierwotny.ToUpper().Trim() == "PART1" || r.PartPierwotny.ToUpper().Trim() == "PART2")
                          && r.historyczny == 1
                          select r.Rozchod).ToList();
            }

            foreach (var w in wyniki) { wynik = wynik + w; }

            return wynik;

        }
    }
}
