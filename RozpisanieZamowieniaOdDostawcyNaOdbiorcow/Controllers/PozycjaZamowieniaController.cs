﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RozpisanieZamowieniaOdDostawcyNaOdbiorcow.Model;

namespace RozpisanieZamowieniaOdDostawcyNaOdbiorcow.Controllers
{
    public static class PozycjaZamowieniaController
    {
        public static List<PozycjaZamowieniaModel> PobierzPozycjeZamowieniaDoDostawcy()
        {
            List<PozycjaZamowieniaModel> pozycjeZamowienia;

            using (NOWASA2018Entities entity = new NOWASA2018Entities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                pozycjeZamowienia = (from pz in entity.POZYCJA_ZAMOWIENIA
                                     join a in entity.ARTYKUL on pz.ID_ARTYKULU equals a.ID_ARTYKULU
                                     where pz.ID_ZAMOWIENIA == ParametryUruchomienioweController.idZamowienia
                                     select new PozycjaZamowieniaModel()
                                     {
                                         POLE1 = pz.POLE1,
                                         POLE2 = pz.POLE2,
                                         POLE3 = pz.POLE3,
                                         POLE4 = pz.POLE4,
                                         POLE5 = pz.POLE5,
                                         POLE6 = pz.POLE6,
                                         POLE7 = pz.POLE7,
                                         POLE8 = pz.POLE8,
                                         POLE9 = pz.POLE9,
                                         POLE10 = pz.POLE10,
                                         ID_ARTYKULU = pz.ID_ARTYKULU,
                                         ID_POZYCJI_ZAMOWIENIA = pz.ID_POZYCJI_ZAMOWIENIA,
                                         JEDNOSTKA = pz.JEDNOSTKA,
                                         KOD_VAT = pz.KOD_VAT,
                                         NR_SERII = pz.NR_SERII,
                                         OPIS = pz.OPIS,
                                         ZAMOWIONO = pz.ZAMOWIONO,
                                         INDEKS_KATALOGOWY = a.INDEKS_KATALOGOWY
                                     }).ToList();
            }

            return pozycjeZamowienia;
        }

        public static List<PozycjaZamowieniaModel> PobierzPozycjeZamowienOdOdbiorcy(List<ZamowienieModel> zamowieniaOdOdbiorcy, decimal idArtykulu)
        {
            List<PozycjaZamowieniaModel> pozycjeZamowieni;

            List<decimal> idZamowien = zamowieniaOdOdbiorcy.Select(x => x.ID_ZAMOWIENIA).ToList();

            using (NOWASA2018Entities entity = new NOWASA2018Entities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                pozycjeZamowieni = (from pz in entity.POZYCJA_ZAMOWIENIA
                                     where pz.ID_ARTYKULU == idArtykulu && idZamowien.Contains(pz.ID_ZAMOWIENIA??0) 
                                     select new PozycjaZamowieniaModel()
                                     {
                                         POLE1 = pz.POLE1,
                                         POLE2 = pz.POLE2,
                                         POLE3 = pz.POLE3,
                                         POLE4 = pz.POLE4,
                                         POLE5 = pz.POLE5,
                                         POLE6 = pz.POLE6,
                                         POLE7 = pz.POLE7,
                                         POLE8 = pz.POLE8,
                                         POLE9 = pz.POLE9,
                                         POLE10 = pz.POLE10,
                                         ID_ARTYKULU = pz.ID_ARTYKULU,
                                         ID_POZYCJI_ZAMOWIENIA = pz.ID_POZYCJI_ZAMOWIENIA,
                                         JEDNOSTKA = pz.JEDNOSTKA,
                                         KOD_VAT = pz.KOD_VAT,
                                         NR_SERII = pz.NR_SERII,
                                         OPIS = pz.OPIS,
                                         ZAMOWIONO = pz.ZAMOWIONO,
                                         ID_ZAMOWIENIA = pz.ID_ZAMOWIENIA
                                     }).ToList();
            }

            return pozycjeZamowieni;
        }
    }
}
