﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RozpisanieZamowieniaOdDostawcyNaOdbiorcow.Model;
namespace RozpisanieZamowieniaOdDostawcyNaOdbiorcow.Controllers
{
    public static class vLokalizacjeController
    {
        public static vLokalizacjaModel PobraniePartuZLokalizacjaPoNazwiePartu(string Part)
        {
            vLokalizacjaModel partZLokalizacjaWMS;

            using (NOWASA2018Entities entity = new NOWASA2018Entities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                partZLokalizacjaWMS = (from p in entity.vLokalizacje
                                       where p.lokalizacja == Part
                                       select new vLokalizacjaModel()
                                       {
                                           idKontrahenta = p.idKontrahenta,
                                           idLokalizacji = p.idLokalizacji,
                                           lokalizacja = p.lokalizacja
                                       }).FirstOrDefault();
            }

            return partZLokalizacjaWMS;
        }

        public static vLokalizacjaModel PobraniePartuZLokalizacjaPoIdKontrahenta(decimal idKontrahenta)
        {
            vLokalizacjaModel partZLokalizacjaWMS;

            using (NOWASA2018Entities entity = new NOWASA2018Entities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                partZLokalizacjaWMS = (from p in entity.vLokalizacje
                                       where p.idKontrahenta == idKontrahenta
                                       select new vLokalizacjaModel()
                                       {
                                           idKontrahenta = p.idKontrahenta,
                                           idLokalizacji = p.idLokalizacji,
                                           lokalizacja = p.lokalizacja
                                       }).FirstOrDefault();
            }

            return partZLokalizacjaWMS;
        }


        public static List<vLokalizacjaModel> PobraniePartowZLokalizacjamiPoIdKontrahenta(decimal idKontrahenta)
        {
            List<vLokalizacjaModel> partyKontrahenta;

            using (NOWASA2018Entities entity = new NOWASA2018Entities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                partyKontrahenta = (from p in entity.vLokalizacje
                                    where p.idKontrahenta == idKontrahenta
                                    select new vLokalizacjaModel()
                                    {
                                        idKontrahenta = p.idKontrahenta,
                                        idLokalizacji = p.idLokalizacji,
                                        lokalizacja = p.lokalizacja
                                    }).ToList();
            }

            return partyKontrahenta;
        }

        public static List<vLokalizacjaModel> PobranieWszystkichPartow()
        {
            List<vLokalizacjaModel> wszystkieParty;

            using (NOWASA2018Entities entity = new NOWASA2018Entities(ParametryUruchomienioweController.connectionStringToModelEntities))
            {
                wszystkieParty = (from p in entity.vLokalizacje
                                    select new vLokalizacjaModel()
                                    {
                                        idKontrahenta = p.idKontrahenta,
                                        idLokalizacji = p.idLokalizacji,
                                        lokalizacja = p.lokalizacja
                                    }).ToList();
            }

            return wszystkieParty;
        }
    }
}
