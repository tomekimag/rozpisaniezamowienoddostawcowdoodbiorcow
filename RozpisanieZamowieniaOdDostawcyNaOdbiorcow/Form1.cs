﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RozpisanieZamowieniaOdDostawcyNaOdbiorcow.Controllers;
using RozpisanieZamowieniaOdDostawcyNaOdbiorcow.Model;

namespace RozpisanieZamowieniaOdDostawcyNaOdbiorcow
{
    public partial class Form1 : Form
    {
        private List<PozycjaZamowieniaModel> listaPozycji;
        private List<ZamowienieModel> listaWybranychZamowien;
        private List<vLokalizacjaModel> listaWszystkichPartow;

        public Form1()
        {
            InitializeComponent();
            PobierzDaneNaFormularz();
            ZaladujDomyslnaListeZamowien();
            ZaladujListePartow();
            //MessageBox.Show($@"idFirmy={ParametryUruchomienioweController.idFirmy};idMagazynu={ParametryUruchomienioweController.idMagazynu};idZamowienia={ParametryUruchomienioweController.idZamowienia};idUzytkownika={ParametryUruchomienioweController.idUzytkownika};");
        }

        private void ZaladujListePartow()
        {
            listaWszystkichPartow = vLokalizacjeController.PobranieWszystkichPartow();
        }

        private void PobierzDaneNaFormularz()
        {
            PobierzDaneZamowienia();
            PobierzPozycjeZamowienia();
        }

        private void ZaladujDomyslnaListeZamowien()
        {
            listaWybranychZamowien = ZamowienieController.PobierzWszystkieZamowieniaZeStatusemOczekujaceNaPlatnosc();
        }

        private void PobierzDaneZamowienia()
        {
            ZamowienieModel zamowienie = ZamowienieController.PobierzZamowienie();

            if (zamowienie != null)
            {
                textBox_kontrahent.Text = zamowienie.KONTRAHENT_NAZWA;
                textBox_numerZamowieniaDoDost.Text = zamowienie.NUMER;
            }
        }

        private void PobierzPozycjeZamowienia()
        {
            dataGridView_pozycjeZamowieniaDoDost.DataSource = listaPozycji = PozycjaZamowieniaController.PobierzPozycjeZamowieniaDoDostawcy();
        }

        private void dataGridView_pozycjeZamowieniaDoDost_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "ID_POZYCJI_ZAMOWIENIA":
                    e.Column.Visible = false;
                    break;
                case "ID_ZAMOWIENIA":
                    e.Column.Visible = false;
                    break;
                case "ID_ARTYKULU":
                    e.Column.Visible = false;
                    break;
                case "POLE1":
                    e.Column.Visible = false;
                    break;
                case "POLE2":
                    e.Column.Visible = false;
                    break;
                case "POLE3":
                    e.Column.Visible = false;
                    break;
                case "POLE4":
                    e.Column.Visible = false;
                    break;
                case "POLE6":
                    e.Column.Visible = false;
                    break;
                case "POLE7":
                    e.Column.Visible = false;
                    break;
                case "POLE8":
                    e.Column.Visible = false;
                    break;
                case "POLE9":
                    e.Column.Visible = false;
                    break;
                case "KOD_VAT":
                    e.Column.Visible = false;
                    break;
                case "POLE10":
                    e.Column.Visible = false;
                    break;
                case "POLE5":
                    e.Column.Visible = true;
                    e.Column.HeaderText = "PART";
                    break;
                default:
                    e.Column.Visible = true;
                    break;
            }
        }

        private void dataGridView_pozycjeZamowieniaDoDost_DataSourceChanged(object sender, EventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            dgv.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgv.MultiSelect = false;
            dgv.AutoResizeColumns();
            dgv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        private void button_anuluj_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form_zamowieniaOdOdbiorcow zamowieniaOdOdbiorcow = new Form_zamowieniaOdOdbiorcow(listaWybranychZamowien);
            zamowieniaOdOdbiorcow.ShowDialog();
            listaWybranychZamowien = zamowieniaOdOdbiorcow.listaWybranychZamowien;
        }

        private void button_rozpiszPart1_Click(object sender, EventArgs e)
        {

        }

        private void button_rozpiszAutomatycznie_Click(object sender, EventArgs e)
        {
            if (listaWybranychZamowien.Count() == 0)
            {
                if (MessageBox.Show("Nie wybrano zamówień do rozpisania. Czy kontynuować?", "Pytanie", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) return;
            }

            C_imag_rozpisanie_partowController.WyczyscTabelkeRozpisuPartow();

            List<PozycjaZamowieniaModel> listaPozycjiPartRoznyOd1i2 = listaPozycji.Where(x => x.POLE5.Trim().ToUpper() != "Part1".ToUpper() && x.POLE5.Trim().ToUpper() != "Part2".ToUpper()).ToList();
            listaPozycjiPartRoznyOd1i2.ForEach(GenerujRozchodDlaPozycjiPrzychodowej);

            List<PozycjaZamowieniaModel> listaPozycjiPartRownyOd1i2 = listaPozycji.Where(x => x.POLE5.Trim().ToUpper() == "Part1".ToUpper() || x.POLE5.Trim().ToUpper() == "Part2".ToUpper()).ToList();
            listaPozycjiPartRownyOd1i2.ForEach(GenerujRozchodDlaPozycjiPrzychodowej);


            //listaPozycji.ForEach(GenerujRozchodDlaPozycjiPrzychodowej);

            button_rozpiszPart2.Enabled = button_zapisz.Enabled = button_zborczaRozpiskaDoExcela.Enabled = true;
        }

        private void GenerujRozchodDlaPozycjiPrzychodowej(PozycjaZamowieniaModel pozycjaPrzychodowa)
        {
            /*
            if (pozycjaPrzychodowa.POLE5.Trim() == "Part1") //Part 1
            {
                vLokalizacjaModel partILokalizacja = vLokalizacjeController.PobraniePartuZLokalizacjaPoNazwiePartu(pozycjaPrzychodowa.POLE5.Trim());
                if (partILokalizacja == null) { MessageBox.Show($@"W WMS brakuje lokalizacji dla PART = {pozycjaPrzychodowa.POLE5.Trim()}", "Pytanie", MessageBoxButtons.OK, MessageBoxIcon.Information); return; }
                C_imag_rozpisanie_partowController.RozpiszLokalizacjePartow(pozycjaPrzychodowa.ID_POZYCJI_ZAMOWIENIA, partILokalizacja.lokalizacja, partILokalizacja.idLokalizacji??0, pozycjaPrzychodowa.ZAMOWIONO);
            }
            else */
            if ((pozycjaPrzychodowa.POLE5.Trim().ToUpper() != "Part1".ToUpper()) && (pozycjaPrzychodowa.POLE5.Trim().ToUpper() != "PART2".ToUpper()))
            {
                vLokalizacjaModel partILokalizacja = vLokalizacjeController.PobraniePartuZLokalizacjaPoNazwiePartu(pozycjaPrzychodowa.POLE5.Trim());
                if (partILokalizacja == null) { MessageBox.Show($@"W WMS brakuje lokalizacji dla PART = {pozycjaPrzychodowa.POLE5.Trim()}", "Pytanie", MessageBoxButtons.OK, MessageBoxIcon.Information); return; }
                C_imag_rozpisanie_partowController.RozpiszLokalizacjePartow(pozycjaPrzychodowa.ID_POZYCJI_ZAMOWIENIA, partILokalizacja.lokalizacja, 
                    partILokalizacja.idLokalizacji??0, pozycjaPrzychodowa.ZAMOWIONO, pozycjaPrzychodowa.POLE5.Trim().ToUpper());
            }
            else if ((pozycjaPrzychodowa.POLE5.Trim().ToUpper() == "PART2".ToUpper()) || (pozycjaPrzychodowa.POLE5.Trim().ToUpper() == "Part1".ToUpper()))//Rozbicie po zamówieniach wybranych.
            {

                List<PozycjaZamowieniaModel> pozycjeZamowienOdOdbiorcy = PozycjaZamowieniaController.PobierzPozycjeZamowienOdOdbiorcy(listaWybranychZamowien, pozycjaPrzychodowa.ID_ARTYKULU??0);

                var kluczeKontrahentow = from z in listaWybranychZamowien
                                         group z by z.ID_KONTRAHENTA into g
                                         select g.Key;

                var listaIlosciPoKontrahentach = kluczeKontrahentow.Select(x => new PozycjeZamowieniaKontrahentaModel() { idKontrahenta = x, iloscSuma = 0 }).ToList();

                foreach (var podsumowanieKontrahena in listaIlosciPoKontrahentach)
                {
                    podsumowanieKontrahena.iloscSuma = (from p in pozycjeZamowienOdOdbiorcy
                                                        join z in listaWybranychZamowien on p.ID_ZAMOWIENIA equals z.ID_ZAMOWIENIA
                                                        //join i in listaIlosciPoKontrahentach on z.ID_KONTRAHENTA equals i.idKontrahenta
                                                        where z.ID_KONTRAHENTA == podsumowanieKontrahena.idKontrahenta
                                                        select p.ZAMOWIONO).Sum();
                }

                decimal iloscDoRozpisania = pozycjaPrzychodowa.ZAMOWIONO;

                foreach(var p in listaIlosciPoKontrahentach.OrderByDescending(x => x.iloscSuma).ToList())
                {
                    //MessageBox.Show($@"idKontrahenta = {p.idKontrahenta}, iloscSuma = {p.iloscSuma}");

                    if (iloscDoRozpisania == 0) break;

                    

                    List<PozycjaZamowieniaModel> listaPozycjiZamowienRozchodowychKontrahenta = (from pz in pozycjeZamowienOdOdbiorcy
                                                                                                join z in listaWybranychZamowien on pz.ID_ZAMOWIENIA equals z.ID_ZAMOWIENIA
                                                                                                where z.ID_KONTRAHENTA == p.idKontrahenta
                                                                                                select pz).ToList();



                    foreach (var pk in listaPozycjiZamowienRozchodowychKontrahenta)
                    {
                        decimal iloscJuzRozpisanaWInnychPartachNizPart1i2 = C_imag_partyController.PobranieIlosciJuzRozpisanejWPartachInnychNiz1i2(p.idKontrahenta ?? 0, pk.ID_ARTYKULU ?? 0);

                        if (iloscJuzRozpisanaWInnychPartachNizPart1i2 > 0) continue;

                        decimal zamowionoWeWszystkichZamowieniachArtykul = listaPozycjiZamowienRozchodowychKontrahenta.Where(x => x.ID_ARTYKULU == pk.ID_ARTYKULU).Sum(x => x.ZAMOWIONO);


                        decimal iloscJuzRozpisanaWInnychKontenerach = C_imag_partyController.PobranieIlosciJuzRozpisanejZPartu1i2NaPozycjeZHistorii(p.idKontrahenta ?? 0, pk.ID_POZYCJI_ZAMOWIENIA);
                        if (pk.ZAMOWIONO - iloscJuzRozpisanaWInnychKontenerach <= 0) continue;

                        decimal rozpiszWPozycji = 0;
                        if (iloscDoRozpisania >= (pk.ZAMOWIONO - iloscJuzRozpisanaWInnychKontenerach))
                        {
                            rozpiszWPozycji = pk.ZAMOWIONO - iloscJuzRozpisanaWInnychKontenerach;
                            iloscDoRozpisania = iloscDoRozpisania - rozpiszWPozycji;
                        }
                        else
                        {
                            rozpiszWPozycji = iloscDoRozpisania;
                            iloscDoRozpisania = 0;
                        }
                        vLokalizacjaModel partILokalizacja = vLokalizacjeController.PobraniePartuZLokalizacjaPoIdKontrahenta(p.idKontrahenta??0);
                        if (partILokalizacja == null) {
                            ZamowienieModel zamowienie = listaWybranychZamowien.Where(x => x.ID_KONTRAHENTA == p.idKontrahenta).FirstOrDefault();
                            if (zamowienie == null) return;
                            MessageBox.Show($@"W WMS brakuje lokalizacji PART dla kontrahenta = {zamowienie.KONTRAHENT_NAZWA}", "Pytanie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                        C_imag_rozpisanie_partowController.RozpiszLokalizacjePartow(pozycjaPrzychodowa.ID_POZYCJI_ZAMOWIENIA, partILokalizacja.lokalizacja, partILokalizacja.idLokalizacji??0, 
                            rozpiszWPozycji, pk.ID_POZYCJI_ZAMOWIENIA, pozycjaPrzychodowa.POLE5.Trim().ToUpper());

                        if (iloscDoRozpisania == 0) break;
                    }   
                }

                if (iloscDoRozpisania > 0)
                {
                    vLokalizacjaModel partILokalizacja = vLokalizacjeController.PobraniePartuZLokalizacjaPoNazwiePartu(pozycjaPrzychodowa.POLE5.Trim());
                    if (partILokalizacja == null) { MessageBox.Show($@"W WMS brakuje lokalizacji dla PART = {pozycjaPrzychodowa.POLE5.Trim()}", "Pytanie", MessageBoxButtons.OK, MessageBoxIcon.Information); return; }
                    C_imag_rozpisanie_partowController.RozpiszLokalizacjePartow(pozycjaPrzychodowa.ID_POZYCJI_ZAMOWIENIA, partILokalizacja.lokalizacja, partILokalizacja.idLokalizacji??0, iloscDoRozpisania, pozycjaPrzychodowa.POLE5.Trim().ToUpper());
                }
            }
        }

        private void button_rozpiszPart2_Click(object sender, EventArgs e)
        {
            if (dataGridView_pozycjeZamowieniaDoDost.SelectedRows == null)
            {
                MessageBox.Show("Wskarz pozycję.", "Imag Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (dataGridView_pozycjeZamowieniaDoDost.SelectedRows.Count != 0)
            {
                decimal idPozycjiZamowieniaPrzychodowej = decimal.Parse(dataGridView_pozycjeZamowieniaDoDost["ID_POZYCJI_ZAMOWIENIA", dataGridView_pozycjeZamowieniaDoDost.SelectedRows[0].Index].Value.ToString());
                string PART = dataGridView_pozycjeZamowieniaDoDost["POLE5", dataGridView_pozycjeZamowieniaDoDost.SelectedRows[0].Index].Value.ToString().Trim();

                Form_rozpiskaPartowNaPozycjiZamPrzychodowego rozpiska = new Form_rozpiskaPartowNaPozycjiZamPrzychodowego(idPozycjiZamowieniaPrzychodowej, PART);
                rozpiska.ShowDialog();
            }
        }

        private void button_zapisz_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Czy chcesz zapisać ustalone zmiany na zamówieniach?", "Pytanie", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                try
                {
                    ZamowienieController.PrzetworzZamowieniePrzychodoweNaPartachILokalizacjach();
                    C_imag_rozpisanie_partowController.HistoryzujZapisyTabelkiRozpisuPartow();

                    MessageBox.Show($@"Zamówienia zostały przetworzone. Moduł zostanie zamknięty.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Close();
                }
                catch (Exception ex)
                {
                    if (ex.InnerException != null)
                        MessageBox.Show($@"Błąd przetwarzania:{ex.Message} {ex.InnerException.Message}", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    else
                        MessageBox.Show($@"Błąd przetwarzania:{ex.Message}", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void button_zborczaRozpiskaDoExcela_Click(object sender, EventArgs e)
        {
            C_imag_rozpisanie_partowController.ZapiszRozpiskeDoPlikuExcel(C_imag_rozpisanie_partowController.ZwrocRozpiskeZDanymiZPozycjiDlaWszystkichPartow(), true);
        }
    }
}
