﻿namespace RozpisanieZamowieniaOdDostawcyNaOdbiorcow
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox_kontrahent = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_numerZamowieniaDoDost = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button_zborczaRozpiskaDoExcela = new System.Windows.Forms.Button();
            this.button_rozpiszAutomatycznie = new System.Windows.Forms.Button();
            this.button_zapisz = new System.Windows.Forms.Button();
            this.button_anuluj = new System.Windows.Forms.Button();
            this.button_rozpiszPart2 = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.tabControl_glowny = new System.Windows.Forms.TabControl();
            this.tabPage_zamowienieDoDostawcy = new System.Windows.Forms.TabPage();
            this.dataGridView_pozycjeZamowieniaDoDost = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tabControl_glowny.SuspendLayout();
            this.tabPage_zamowienieDoDostawcy.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_pozycjeZamowieniaDoDost)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.textBox_kontrahent);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.textBox_numerZamowieniaDoDost);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(990, 64);
            this.panel1.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(800, 18);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(168, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Zamówienie od odbiorców";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox_kontrahent
            // 
            this.textBox_kontrahent.Enabled = false;
            this.textBox_kontrahent.Location = new System.Drawing.Point(320, 20);
            this.textBox_kontrahent.Name = "textBox_kontrahent";
            this.textBox_kontrahent.Size = new System.Drawing.Size(394, 20);
            this.textBox_kontrahent.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(273, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Numer:";
            // 
            // textBox_numerZamowieniaDoDost
            // 
            this.textBox_numerZamowieniaDoDost.Enabled = false;
            this.textBox_numerZamowieniaDoDost.Location = new System.Drawing.Point(73, 20);
            this.textBox_numerZamowieniaDoDost.Name = "textBox_numerZamowieniaDoDost";
            this.textBox_numerZamowieniaDoDost.Size = new System.Drawing.Size(167, 20);
            this.textBox_numerZamowieniaDoDost.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Numer:";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.button_zborczaRozpiskaDoExcela);
            this.panel2.Controls.Add(this.button_rozpiszAutomatycznie);
            this.panel2.Controls.Add(this.button_zapisz);
            this.panel2.Controls.Add(this.button_anuluj);
            this.panel2.Controls.Add(this.button_rozpiszPart2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 431);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(990, 79);
            this.panel2.TabIndex = 1;
            // 
            // button_zborczaRozpiskaDoExcela
            // 
            this.button_zborczaRozpiskaDoExcela.Enabled = false;
            this.button_zborczaRozpiskaDoExcela.Location = new System.Drawing.Point(435, 26);
            this.button_zborczaRozpiskaDoExcela.Name = "button_zborczaRozpiskaDoExcela";
            this.button_zborczaRozpiskaDoExcela.Size = new System.Drawing.Size(190, 23);
            this.button_zborczaRozpiskaDoExcela.TabIndex = 6;
            this.button_zborczaRozpiskaDoExcela.Text = "Szczegóły rozpis. poz. do Excel";
            this.button_zborczaRozpiskaDoExcela.UseVisualStyleBackColor = true;
            this.button_zborczaRozpiskaDoExcela.Click += new System.EventHandler(this.button_zborczaRozpiskaDoExcela_Click);
            // 
            // button_rozpiszAutomatycznie
            // 
            this.button_rozpiszAutomatycznie.Location = new System.Drawing.Point(29, 26);
            this.button_rozpiszAutomatycznie.Name = "button_rozpiszAutomatycznie";
            this.button_rozpiszAutomatycznie.Size = new System.Drawing.Size(204, 23);
            this.button_rozpiszAutomatycznie.TabIndex = 5;
            this.button_rozpiszAutomatycznie.Text = "Rozpisz automatycznie";
            this.button_rozpiszAutomatycznie.UseVisualStyleBackColor = true;
            this.button_rozpiszAutomatycznie.Click += new System.EventHandler(this.button_rozpiszAutomatycznie_Click);
            // 
            // button_zapisz
            // 
            this.button_zapisz.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_zapisz.Enabled = false;
            this.button_zapisz.Location = new System.Drawing.Point(800, 26);
            this.button_zapisz.Name = "button_zapisz";
            this.button_zapisz.Size = new System.Drawing.Size(75, 23);
            this.button_zapisz.TabIndex = 4;
            this.button_zapisz.Text = "Zapisz";
            this.button_zapisz.UseVisualStyleBackColor = true;
            this.button_zapisz.Click += new System.EventHandler(this.button_zapisz_Click);
            // 
            // button_anuluj
            // 
            this.button_anuluj.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_anuluj.Location = new System.Drawing.Point(881, 26);
            this.button_anuluj.Name = "button_anuluj";
            this.button_anuluj.Size = new System.Drawing.Size(75, 23);
            this.button_anuluj.TabIndex = 3;
            this.button_anuluj.Text = "Anuluj";
            this.button_anuluj.UseVisualStyleBackColor = true;
            this.button_anuluj.Click += new System.EventHandler(this.button_anuluj_Click);
            // 
            // button_rozpiszPart2
            // 
            this.button_rozpiszPart2.Enabled = false;
            this.button_rozpiszPart2.Location = new System.Drawing.Point(239, 26);
            this.button_rozpiszPart2.Name = "button_rozpiszPart2";
            this.button_rozpiszPart2.Size = new System.Drawing.Size(190, 23);
            this.button_rozpiszPart2.TabIndex = 1;
            this.button_rozpiszPart2.Text = "Szczegóły rozpisania pozycji";
            this.button_rozpiszPart2.UseVisualStyleBackColor = true;
            this.button_rozpiszPart2.Click += new System.EventHandler(this.button_rozpiszPart2_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.tabControl_glowny);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 64);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(990, 367);
            this.panel3.TabIndex = 1;
            // 
            // tabControl_glowny
            // 
            this.tabControl_glowny.Controls.Add(this.tabPage_zamowienieDoDostawcy);
            this.tabControl_glowny.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl_glowny.Location = new System.Drawing.Point(0, 0);
            this.tabControl_glowny.Name = "tabControl_glowny";
            this.tabControl_glowny.SelectedIndex = 0;
            this.tabControl_glowny.Size = new System.Drawing.Size(990, 367);
            this.tabControl_glowny.TabIndex = 1;
            // 
            // tabPage_zamowienieDoDostawcy
            // 
            this.tabPage_zamowienieDoDostawcy.Controls.Add(this.dataGridView_pozycjeZamowieniaDoDost);
            this.tabPage_zamowienieDoDostawcy.Location = new System.Drawing.Point(4, 22);
            this.tabPage_zamowienieDoDostawcy.Name = "tabPage_zamowienieDoDostawcy";
            this.tabPage_zamowienieDoDostawcy.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_zamowienieDoDostawcy.Size = new System.Drawing.Size(982, 341);
            this.tabPage_zamowienieDoDostawcy.TabIndex = 0;
            this.tabPage_zamowienieDoDostawcy.Text = "Zamówienie do dostawcy";
            this.tabPage_zamowienieDoDostawcy.UseVisualStyleBackColor = true;
            // 
            // dataGridView_pozycjeZamowieniaDoDost
            // 
            this.dataGridView_pozycjeZamowieniaDoDost.AllowUserToAddRows = false;
            this.dataGridView_pozycjeZamowieniaDoDost.AllowUserToDeleteRows = false;
            this.dataGridView_pozycjeZamowieniaDoDost.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_pozycjeZamowieniaDoDost.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView_pozycjeZamowieniaDoDost.Location = new System.Drawing.Point(3, 3);
            this.dataGridView_pozycjeZamowieniaDoDost.Name = "dataGridView_pozycjeZamowieniaDoDost";
            this.dataGridView_pozycjeZamowieniaDoDost.ReadOnly = true;
            this.dataGridView_pozycjeZamowieniaDoDost.Size = new System.Drawing.Size(976, 335);
            this.dataGridView_pozycjeZamowieniaDoDost.TabIndex = 1;
            this.dataGridView_pozycjeZamowieniaDoDost.DataSourceChanged += new System.EventHandler(this.dataGridView_pozycjeZamowieniaDoDost_DataSourceChanged);
            this.dataGridView_pozycjeZamowieniaDoDost.ColumnAdded += new System.Windows.Forms.DataGridViewColumnEventHandler(this.dataGridView_pozycjeZamowieniaDoDost_ColumnAdded);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(990, 510);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Rozpisywanie zamówień przychodowych na rozchodowe";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.tabControl_glowny.ResumeLayout(false);
            this.tabPage_zamowienieDoDostawcy.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_pozycjeZamowieniaDoDost)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox textBox_kontrahent;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox_numerZamowieniaDoDost;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button_rozpiszPart2;
        private System.Windows.Forms.Button button_zapisz;
        private System.Windows.Forms.Button button_anuluj;
        private System.Windows.Forms.TabControl tabControl_glowny;
        private System.Windows.Forms.TabPage tabPage_zamowienieDoDostawcy;
        private System.Windows.Forms.DataGridView dataGridView_pozycjeZamowieniaDoDost;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button_rozpiszAutomatycznie;
        private System.Windows.Forms.Button button_zborczaRozpiskaDoExcela;
    }
}

