﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RozpisanieZamowieniaOdDostawcyNaOdbiorcow.Model
{
    public class ZamowienieModel
    {
        public decimal ID_ZAMOWIENIA { get; set; }
        public Nullable<decimal> ID_KONTRAHENTA { get; set; }
        public decimal ID_FIRMY { get; set; }
        public decimal ID_MAGAZYNU { get; set; }
        public string NUMER { get; set; }
        public string NR_ZAMOWIENIA_KLIENTA { get; set; }
        public Nullable<int> DATA { get; set; }
        public DateTime DATA_ZAMOWIENIA { get; set; }
        public string POLE1 { get; set; }
        public string POLE2 { get; set; }
        public string POLE3 { get; set; }
        public string POLE4 { get; set; }
        public string POLE5 { get; set; }
        public string POLE6 { get; set; }
        public string POLE7 { get; set; }
        public string POLE8 { get; set; }
        public string POLE9 { get; set; }
        public string POLE10 { get; set; }
        public string KONTRAHENT_NAZWA { get; set; }
    }
}
