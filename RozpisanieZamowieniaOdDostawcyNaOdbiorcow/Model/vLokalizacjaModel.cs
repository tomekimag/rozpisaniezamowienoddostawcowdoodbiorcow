﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RozpisanieZamowieniaOdDostawcyNaOdbiorcow.Model
{
    public class vLokalizacjaModel
    {
        public Nullable<long> idLokalizacji { get; set; }
        public string lokalizacja { get; set; }
        public Nullable<long> idKontrahenta { get; set; }
    }
}
