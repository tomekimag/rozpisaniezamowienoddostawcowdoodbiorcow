﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RozpisanieZamowieniaOdDostawcyNaOdbiorcow.Model
{
    public class PozycjaZamowieniaModel
    {
        public decimal ID_POZYCJI_ZAMOWIENIA { get; set; }
        public Nullable<decimal> ID_ARTYKULU { get; set; }

        /// <summary>
        /// Pole pochodzi z tabeli Artykul.
        /// </summary>
        public string INDEKS_KATALOGOWY { get; set; }

        public string KOD_VAT { get; set; }
        public decimal ZAMOWIONO { get; set; }
        public string JEDNOSTKA { get; set; }
        public string OPIS { get; set; }
        public string POLE1 { get; set; }
        public string POLE2 { get; set; }
        public string POLE3 { get; set; }
        public string POLE4 { get; set; }
        public string POLE5 { get; set; }
        public string POLE6 { get; set; }
        public string POLE7 { get; set; }
        public string POLE8 { get; set; }
        public string POLE9 { get; set; }
        public string POLE10 { get; set; }
        public string NR_SERII { get; set; }
        public Nullable<decimal> ID_ZAMOWIENIA { get; set; }
    }
}
