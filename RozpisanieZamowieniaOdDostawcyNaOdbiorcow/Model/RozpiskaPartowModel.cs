﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RozpisanieZamowieniaOdDostawcyNaOdbiorcow.Model
{
    public class RozpiskaPartowModel
    {
        public long id { get; set; }
        public decimal id_poz_zam_przychod { get; set; }
        public decimal idLokalizacji { get; set; }
        public string Part { get; set; }
        public decimal Rozchod { get; set; }
        
        /// <summary>
        /// Poniższe pole jest z pozycji zamówienia rozchodowego.
        /// </summary>
        public decimal ZAMOWIONO { get; set; }

        /// <summary>
        /// Poniższe dwa pola pochodzą z zamówienia.
        /// </summary>
        public string NUMER { get; set; }
        public string KONTRAHENT_NAZWA { get; set; }

        public Nullable<decimal> id_poz_zam_rozchod { get; set; }
        public decimal id_uzytkownika { get; set; }


        /// <summary>
        /// Poniżej dwa pola z tabeli artykul.
        /// </summary>
        public string NAZWA { get; set; }
        public string INDEKS_KATALOGOWY { get; set; }
    }
}
