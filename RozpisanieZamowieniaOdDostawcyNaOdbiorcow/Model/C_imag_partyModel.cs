﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RozpisanieZamowieniaOdDostawcyNaOdbiorcow.Model
{
    public class C_imag_partyModel
    {
        public int id { get; set; }
        public decimal id_kontrahenta { get; set; }
        public decimal id_lokalizacji_WMS { get; set; }
        public string part { get; set; }
    }
}
