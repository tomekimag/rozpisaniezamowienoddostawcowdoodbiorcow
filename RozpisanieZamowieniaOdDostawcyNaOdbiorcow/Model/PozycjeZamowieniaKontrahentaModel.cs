﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RozpisanieZamowieniaOdDostawcyNaOdbiorcow.Model
{
    public class PozycjeZamowieniaKontrahentaModel
    {
        public Nullable<decimal> idKontrahenta { get; set; }
        public decimal iloscSuma { get; set; }
    }
}
