﻿namespace RozpisanieZamowieniaOdDostawcyNaOdbiorcow
{
    partial class Form_partyKontrahenta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.button_wybierz = new System.Windows.Forms.Button();
            this.button_anuluj = new System.Windows.Forms.Button();
            this.dataGridView_listaPartowKontrahenta = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_listaPartowKontrahenta)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button_wybierz);
            this.panel1.Controls.Add(this.button_anuluj);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 382);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(586, 82);
            this.panel1.TabIndex = 0;
            // 
            // button_wybierz
            // 
            this.button_wybierz.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_wybierz.Location = new System.Drawing.Point(394, 28);
            this.button_wybierz.Name = "button_wybierz";
            this.button_wybierz.Size = new System.Drawing.Size(75, 23);
            this.button_wybierz.TabIndex = 1;
            this.button_wybierz.Text = "Wybierz";
            this.button_wybierz.UseVisualStyleBackColor = true;
            this.button_wybierz.Click += new System.EventHandler(this.button_wybierz_Click);
            // 
            // button_anuluj
            // 
            this.button_anuluj.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_anuluj.Location = new System.Drawing.Point(475, 28);
            this.button_anuluj.Name = "button_anuluj";
            this.button_anuluj.Size = new System.Drawing.Size(75, 23);
            this.button_anuluj.TabIndex = 0;
            this.button_anuluj.Text = "Anuluj";
            this.button_anuluj.UseVisualStyleBackColor = true;
            this.button_anuluj.Click += new System.EventHandler(this.button_anuluj_Click);
            // 
            // dataGridView_listaPartowKontrahenta
            // 
            this.dataGridView_listaPartowKontrahenta.AllowUserToAddRows = false;
            this.dataGridView_listaPartowKontrahenta.AllowUserToDeleteRows = false;
            this.dataGridView_listaPartowKontrahenta.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_listaPartowKontrahenta.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView_listaPartowKontrahenta.Location = new System.Drawing.Point(0, 0);
            this.dataGridView_listaPartowKontrahenta.Name = "dataGridView_listaPartowKontrahenta";
            this.dataGridView_listaPartowKontrahenta.ReadOnly = true;
            this.dataGridView_listaPartowKontrahenta.Size = new System.Drawing.Size(586, 382);
            this.dataGridView_listaPartowKontrahenta.TabIndex = 1;
            this.dataGridView_listaPartowKontrahenta.DataSourceChanged += new System.EventHandler(this.dataGridView_listaPartowKontrahenta_DataSourceChanged);
            this.dataGridView_listaPartowKontrahenta.ColumnAdded += new System.Windows.Forms.DataGridViewColumnEventHandler(this.dataGridView_listaPartowKontrahenta_ColumnAdded);
            // 
            // Form_partyKontrahenta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(586, 464);
            this.Controls.Add(this.dataGridView_listaPartowKontrahenta);
            this.Controls.Add(this.panel1);
            this.Name = "Form_partyKontrahenta";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Pozycja rozpiski";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_listaPartowKontrahenta)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button_wybierz;
        private System.Windows.Forms.Button button_anuluj;
        private System.Windows.Forms.DataGridView dataGridView_listaPartowKontrahenta;
    }
}