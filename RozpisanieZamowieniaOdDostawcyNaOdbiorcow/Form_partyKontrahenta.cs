﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RozpisanieZamowieniaOdDostawcyNaOdbiorcow.Model;
using RozpisanieZamowieniaOdDostawcyNaOdbiorcow.Controllers;

namespace RozpisanieZamowieniaOdDostawcyNaOdbiorcow
{
    public partial class Form_partyKontrahenta : Form
    {
        private decimal idKontrahenta;
        private List<vLokalizacjaModel> party;
        public vLokalizacjaModel wybranyPart;

        public Form_partyKontrahenta(decimal idKontrahenta)
        {
            InitializeComponent();
            this.idKontrahenta = idKontrahenta;
            ZaladujDaneDoSiatki();
        }

        private void ZaladujDaneDoSiatki()
        {
            dataGridView_listaPartowKontrahenta.DataSource = party = vLokalizacjeController.PobraniePartowZLokalizacjamiPoIdKontrahenta(idKontrahenta);
        }

        private void button_anuluj_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void dataGridView_listaPartowKontrahenta_DataSourceChanged(object sender, EventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            dgv.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgv.MultiSelect = false;
            dgv.AutoResizeColumns();
            dgv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        private void dataGridView_listaPartowKontrahenta_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "idKontrahenta":
                    e.Column.Visible = false;
                    break;
                default:
                    e.Column.Visible = true;
                    break;
            }
        }

        private void button_wybierz_Click(object sender, EventArgs e)
        {
            if (dataGridView_listaPartowKontrahenta.SelectedRows == null)
            {
                MessageBox.Show("Wskarz pozycje.", "Imag Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (dataGridView_listaPartowKontrahenta.SelectedRows.Count != 0)
            {
                /*        public Nullable<long> idLokalizacji { get; set; }
        public string lokalizacja { get; set; }
        public Nullable<long> idKontrahenta { get; set; }
*/
                long idLokalizacji = long.Parse(dataGridView_listaPartowKontrahenta["idLokalizacji", dataGridView_listaPartowKontrahenta.SelectedRows[0].Index].Value.ToString());
                string lokalizacja = dataGridView_listaPartowKontrahenta["lokalizacja", dataGridView_listaPartowKontrahenta.SelectedRows[0].Index].Value.ToString();
                long idKontrahenta = long.Parse(dataGridView_listaPartowKontrahenta["idKontrahenta", dataGridView_listaPartowKontrahenta.SelectedRows[0].Index].Value.ToString());

                wybranyPart = party.Where(x => x.idKontrahenta == idKontrahenta && x.idLokalizacji == idLokalizacji && x.lokalizacja == lokalizacja).FirstOrDefault();
                if (wybranyPart != null)
                {
                    this.DialogResult = DialogResult.Yes;
                    Close();
                }
            }
        }
    }
}
