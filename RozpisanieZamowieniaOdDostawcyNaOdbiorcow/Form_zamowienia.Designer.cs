﻿namespace RozpisanieZamowieniaOdDostawcyNaOdbiorcow
{
    partial class Form_zamowienia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.button_wszystko = new System.Windows.Forms.Button();
            this.button_szukaj = new System.Windows.Forms.Button();
            this.textBox_szukanyCiag = new System.Windows.Forms.TextBox();
            this.comboBox_pole = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button_wyjdz = new System.Windows.Forms.Button();
            this.button_wybierz = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.dataGridView_wszystkieZamOdOdbiorcow = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_wszystkieZamOdOdbiorcow)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button_wszystko);
            this.panel1.Controls.Add(this.button_szukaj);
            this.panel1.Controls.Add(this.textBox_szukanyCiag);
            this.panel1.Controls.Add(this.comboBox_pole);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(833, 56);
            this.panel1.TabIndex = 0;
            // 
            // button_wszystko
            // 
            this.button_wszystko.Location = new System.Drawing.Point(636, 16);
            this.button_wszystko.Name = "button_wszystko";
            this.button_wszystko.Size = new System.Drawing.Size(75, 23);
            this.button_wszystko.TabIndex = 4;
            this.button_wszystko.Text = "Wszystko";
            this.button_wszystko.UseVisualStyleBackColor = true;
            this.button_wszystko.Click += new System.EventHandler(this.button_wszystko_Click);
            // 
            // button_szukaj
            // 
            this.button_szukaj.Location = new System.Drawing.Point(555, 16);
            this.button_szukaj.Name = "button_szukaj";
            this.button_szukaj.Size = new System.Drawing.Size(75, 23);
            this.button_szukaj.TabIndex = 3;
            this.button_szukaj.Text = "Szukaj";
            this.button_szukaj.UseVisualStyleBackColor = true;
            this.button_szukaj.Click += new System.EventHandler(this.button_szukaj_Click);
            // 
            // textBox_szukanyCiag
            // 
            this.textBox_szukanyCiag.Location = new System.Drawing.Point(344, 18);
            this.textBox_szukanyCiag.Name = "textBox_szukanyCiag";
            this.textBox_szukanyCiag.Size = new System.Drawing.Size(205, 20);
            this.textBox_szukanyCiag.TabIndex = 2;
            // 
            // comboBox_pole
            // 
            this.comboBox_pole.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_pole.FormattingEnabled = true;
            this.comboBox_pole.Items.AddRange(new object[] {
            "Numer",
            "Kontrahent",
            "NR_ZAMOWIENIA_KLIENTA"});
            this.comboBox_pole.Location = new System.Drawing.Point(106, 18);
            this.comboBox_pole.Name = "comboBox_pole";
            this.comboBox_pole.Size = new System.Drawing.Size(232, 21);
            this.comboBox_pole.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Wyszukiwanie:";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.button_wyjdz);
            this.panel2.Controls.Add(this.button_wybierz);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 435);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(833, 78);
            this.panel2.TabIndex = 1;
            // 
            // button_wyjdz
            // 
            this.button_wyjdz.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_wyjdz.Location = new System.Drawing.Point(732, 27);
            this.button_wyjdz.Name = "button_wyjdz";
            this.button_wyjdz.Size = new System.Drawing.Size(75, 23);
            this.button_wyjdz.TabIndex = 1;
            this.button_wyjdz.Text = "Anuluj";
            this.button_wyjdz.UseVisualStyleBackColor = true;
            this.button_wyjdz.Click += new System.EventHandler(this.button_wyjdz_Click);
            // 
            // button_wybierz
            // 
            this.button_wybierz.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_wybierz.Location = new System.Drawing.Point(651, 27);
            this.button_wybierz.Name = "button_wybierz";
            this.button_wybierz.Size = new System.Drawing.Size(75, 23);
            this.button_wybierz.TabIndex = 0;
            this.button_wybierz.Text = "Wybierz";
            this.button_wybierz.UseVisualStyleBackColor = true;
            this.button_wybierz.Click += new System.EventHandler(this.button_wybierz_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dataGridView_wszystkieZamOdOdbiorcow);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 56);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(833, 379);
            this.panel3.TabIndex = 1;
            // 
            // dataGridView_wszystkieZamOdOdbiorcow
            // 
            this.dataGridView_wszystkieZamOdOdbiorcow.AllowUserToAddRows = false;
            this.dataGridView_wszystkieZamOdOdbiorcow.AllowUserToDeleteRows = false;
            this.dataGridView_wszystkieZamOdOdbiorcow.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_wszystkieZamOdOdbiorcow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView_wszystkieZamOdOdbiorcow.Location = new System.Drawing.Point(0, 0);
            this.dataGridView_wszystkieZamOdOdbiorcow.Name = "dataGridView_wszystkieZamOdOdbiorcow";
            this.dataGridView_wszystkieZamOdOdbiorcow.ReadOnly = true;
            this.dataGridView_wszystkieZamOdOdbiorcow.Size = new System.Drawing.Size(833, 379);
            this.dataGridView_wszystkieZamOdOdbiorcow.TabIndex = 0;
            this.dataGridView_wszystkieZamOdOdbiorcow.DataSourceChanged += new System.EventHandler(this.dataGridView_wszystkieZamOdOdbiorcow_DataSourceChanged);
            this.dataGridView_wszystkieZamOdOdbiorcow.ColumnAdded += new System.Windows.Forms.DataGridViewColumnEventHandler(this.dataGridView_wszystkieZamOdOdbiorcow_ColumnAdded);
            // 
            // Form_zamowienia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(833, 513);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Form_zamowienia";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Zamówienia od odbiorcy w WAPRO Magu";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_wszystkieZamOdOdbiorcow)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridView dataGridView_wszystkieZamOdOdbiorcow;
        private System.Windows.Forms.Button button_wyjdz;
        private System.Windows.Forms.Button button_wybierz;
        private System.Windows.Forms.Button button_szukaj;
        private System.Windows.Forms.TextBox textBox_szukanyCiag;
        private System.Windows.Forms.ComboBox comboBox_pole;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button_wszystko;
    }
}