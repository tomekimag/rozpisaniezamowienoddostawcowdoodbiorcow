﻿namespace RozpisanieZamowieniaOdDostawcyNaOdbiorcow
{
    partial class Form_zamowieniaOdOdbiorcow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.button_zamknij = new System.Windows.Forms.Button();
            this.button_usun = new System.Windows.Forms.Button();
            this.button_dodaj = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridView_zamowieniaOdOdbiorcow = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_zamowieniaOdOdbiorcow)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button_zamknij);
            this.panel1.Controls.Add(this.button_usun);
            this.panel1.Controls.Add(this.button_dodaj);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 431);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(926, 64);
            this.panel1.TabIndex = 0;
            // 
            // button_zamknij
            // 
            this.button_zamknij.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_zamknij.Location = new System.Drawing.Point(820, 18);
            this.button_zamknij.Name = "button_zamknij";
            this.button_zamknij.Size = new System.Drawing.Size(75, 23);
            this.button_zamknij.TabIndex = 2;
            this.button_zamknij.Text = "Zamknij";
            this.button_zamknij.UseVisualStyleBackColor = true;
            this.button_zamknij.Click += new System.EventHandler(this.button_zamknij_Click);
            // 
            // button_usun
            // 
            this.button_usun.Location = new System.Drawing.Point(104, 18);
            this.button_usun.Name = "button_usun";
            this.button_usun.Size = new System.Drawing.Size(75, 23);
            this.button_usun.TabIndex = 1;
            this.button_usun.Text = "Usuń";
            this.button_usun.UseVisualStyleBackColor = true;
            this.button_usun.Click += new System.EventHandler(this.button_usun_Click);
            // 
            // button_dodaj
            // 
            this.button_dodaj.Location = new System.Drawing.Point(23, 18);
            this.button_dodaj.Name = "button_dodaj";
            this.button_dodaj.Size = new System.Drawing.Size(75, 23);
            this.button_dodaj.TabIndex = 0;
            this.button_dodaj.Text = "Dodaj";
            this.button_dodaj.UseVisualStyleBackColor = true;
            this.button_dodaj.Click += new System.EventHandler(this.button_dodaj_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dataGridView_zamowieniaOdOdbiorcow);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(926, 431);
            this.panel2.TabIndex = 1;
            // 
            // dataGridView_zamowieniaOdOdbiorcow
            // 
            this.dataGridView_zamowieniaOdOdbiorcow.AllowUserToAddRows = false;
            this.dataGridView_zamowieniaOdOdbiorcow.AllowUserToDeleteRows = false;
            this.dataGridView_zamowieniaOdOdbiorcow.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_zamowieniaOdOdbiorcow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView_zamowieniaOdOdbiorcow.Location = new System.Drawing.Point(0, 0);
            this.dataGridView_zamowieniaOdOdbiorcow.Name = "dataGridView_zamowieniaOdOdbiorcow";
            this.dataGridView_zamowieniaOdOdbiorcow.ReadOnly = true;
            this.dataGridView_zamowieniaOdOdbiorcow.Size = new System.Drawing.Size(926, 431);
            this.dataGridView_zamowieniaOdOdbiorcow.TabIndex = 0;
            this.dataGridView_zamowieniaOdOdbiorcow.DataSourceChanged += new System.EventHandler(this.dataGridView_zamowieniaOdOdbiorcow_DataSourceChanged);
            this.dataGridView_zamowieniaOdOdbiorcow.ColumnAdded += new System.Windows.Forms.DataGridViewColumnEventHandler(this.dataGridView_zamowieniaOdOdbiorcow_ColumnAdded);
            this.dataGridView_zamowieniaOdOdbiorcow.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridView_zamowieniaOdOdbiorcow_DataError);
            // 
            // Form_zamowieniaOdOdbiorcow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(926, 495);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Form_zamowieniaOdOdbiorcow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Wybrane zamówienia od odbiorców";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_zamowieniaOdOdbiorcow)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button_zamknij;
        private System.Windows.Forms.Button button_usun;
        private System.Windows.Forms.Button button_dodaj;
        private System.Windows.Forms.DataGridView dataGridView_zamowieniaOdOdbiorcow;
    }
}