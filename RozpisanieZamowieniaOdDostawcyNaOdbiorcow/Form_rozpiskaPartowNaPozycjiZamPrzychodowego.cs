﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RozpisanieZamowieniaOdDostawcyNaOdbiorcow.Model;
using RozpisanieZamowieniaOdDostawcyNaOdbiorcow.Controllers;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace RozpisanieZamowieniaOdDostawcyNaOdbiorcow
{
    public partial class Form_rozpiskaPartowNaPozycjiZamPrzychodowego : Form
    {
        private decimal idPozycjiZamowieniaPrzychodowego = 0;
        private string part = string.Empty;
        private List<RozpiskaPartowModel> listaRozpiski;

        public Form_rozpiskaPartowNaPozycjiZamPrzychodowego(decimal idPozycjiZamowieniaPrzychodowego, string part)
        {
            InitializeComponent();
            this.idPozycjiZamowieniaPrzychodowego = idPozycjiZamowieniaPrzychodowego;
            this.part = part;
            ZaladujSiatkeDanymi();
        }

        private void ZaladujSiatkeDanymi()
        {
            listaRozpiski = C_imag_rozpisanie_partowController.ZwrocRozpiskeZDanymiZPozycjiDlaPart1lub2_poPozycjiPrzychodowej(idPozycjiZamowieniaPrzychodowego);

            numericUpDown_rozpisanoSuma.Value = (from r in listaRozpiski
                                                 select r.Rozchod).Sum();

            numericUpDown_zamowionoSuma.Value = (from r in listaRozpiski
                                                 select r.ZAMOWIONO).Sum();

            dataGridView_rozpiskaPartow.DataSource = listaRozpiski;
        }

        private void dataGridView_rozpiskaPartow_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            switch (e.Column.Name)
            {
                case "NAZWA":
                    e.Column.Visible = false;
                    break;
                case "INDEKS_KATALOGOWY":
                    e.Column.Visible = false;
                    break;
                case "id":
                    e.Column.Visible = false;
                    break;
                case "id_poz_zam_przychod":
                    e.Column.Visible = false;
                    break;
                case "id_poz_zam_rozchod":
                    e.Column.Visible = false;
                    break;
                case "id_uzytkownika":
                    e.Column.Visible = false;
                    break;
                default:
                    e.Column.Visible = true;
                    break;
            }
        }

        private void dataGridView_rozpiskaPartow_DataSourceChanged(object sender, EventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            dgv.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgv.MultiSelect = false;
            dgv.AutoResizeColumns();
            dgv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        private void button_zamknij_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button_edytuj_Click(object sender, EventArgs e)
        {
            if (dataGridView_rozpiskaPartow.SelectedRows == null)
            {
                MessageBox.Show("Wskarz pozycje.", "Imag Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (dataGridView_rozpiskaPartow.SelectedRows.Count != 0)
            {
                long id = long.Parse(dataGridView_rozpiskaPartow["id", dataGridView_rozpiskaPartow.SelectedRows[0].Index].Value.ToString());
                RozpiskaPartowModel rozpiska = listaRozpiski.Where(x => x.id == id).FirstOrDefault();
                if (rozpiska != null)
                {
                    decimal idKontrahenta = 0;
                    if (rozpiska.id_poz_zam_rozchod != null)
                    {
                        ZamowienieModel zamowienieRozchodowe = ZamowienieController.PobierzZamowieniePoIdPozycjiZamowienia(rozpiska.id_poz_zam_rozchod??0);
                        idKontrahenta = zamowienieRozchodowe.ID_KONTRAHENTA??0;
                    }
                    else
                    {
                        idKontrahenta = ParametryUruchomienioweController.idKontrahentaNowa;
                    }

                    Form_partyKontrahenta partyKontrahenta = new Form_partyKontrahenta(idKontrahenta);
                    if (partyKontrahenta.ShowDialog() == DialogResult.Yes)
                    {
                        vLokalizacjaModel wybranyPart = partyKontrahenta.wybranyPart;
                        C_imag_rozpisanie_partowController.ZamienLokalizacjeIPartWRozpisce(id, wybranyPart);
                        ZaladujSiatkeDanymi();
                    }
                }
            }
        }

        private void button_eksportDoExcela_Click(object sender, EventArgs e)
        {
            C_imag_rozpisanie_partowController.ZapiszRozpiskeDoPlikuExcel(listaRozpiski);
        }
    }
}
